import React, {Component} from 'react';
import './App.css';
import {Route, BrowserRouter as Router, Switch } from 'react-router-dom';

//components
import Home from './components/Home';
import Login from './components/Login';
import Billing from './components/Billing';
import Household from './components/Household';
import Payment from './components/Payment';
import Settings from './components/Settings';
import AddHousehold from './components/AddHousehold';
import ViewHousehold from './components/ViewHousehold';
import CreateBill from './components/CreateBill'
import BillHousehold from './components/BillHousehold'
import ViewBilling from './components/ViewBilling'
import Ledger from './components/Ledger'
import Soa from './components/Soa'
import Reports from './components/Reports'
class App extends Component {
  constructor(props){
    super(props);
    this.state = {
      loggedIn : false
    }
  }

  render() {
      return (
      
        <Router>
          <div className="App">
              <Switch>
                <Route path="/" exact render={props => (
                    <Login {...props} state = {this.state}/>
                )}/>
                <Route path="/Home" render = {props => (
                  <Home {...props} state = {this.state} />
                )}/>
                <Route path="/Billing" render = {props => (
                  <Billing {...props} state = {this.state} />
                )}/>
                <Route path="/Household" render = {props => (
                  <Household {...props} state = {this.state}></Household>
                )}/>

                <Route path="/Payment" render = {props => (
                  <Payment {...props} state = {this.state}></Payment>
                )}/>

                <Route path="/Settings" render = {props => (
                    <Settings {...props} state = {this.state}></Settings>
                )}/>

                <Route path="/AddHousehold" render = {props => (
                    <AddHousehold {...props} state = {this.state}></AddHousehold>
                )}/>
                <Route path="/ViewHousehold" render = {props => (
                    <ViewHousehold {...props} state = {this.state}></ViewHousehold>
                )}/>
                <Route path="/CreateBill" render = {props => (
                    <CreateBill {...props} state = {this.state}></CreateBill>
                )}/>
                <Route path="/BillHousehold" render = {props => (
                    <BillHousehold {...props} state = {this.state}></BillHousehold>
                )}/>
                <Route path="/ViewBilling" render = {props => (
                    <ViewBilling {...props} state = {this.state}></ViewBilling>
                )}/>
                <Route path="/Ledger" render = {props => (
                    <Ledger {...props} state = {this.state}></Ledger>
                )}/>
                <Route path="/Soa" render = {props => (
                    <Soa {...props} state = {this.state}></Soa>
                )}/>
                <Route path="/Reports" render = {props => (
                    <Reports {...props} state = {this.state}></Reports>
                )}/>
              </Switch>
          </div>
        </Router>
      );
    
  }
}

 

export default App;
