import React, {Component} from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/js/bootstrap.min.js';
import 'jquery/dist/jquery.min.js';
import 'popper.js/dist/umd/popper.min.js'
import $ from 'jquery';
import {Link} from 'react-router-dom';
import  {Button, Form, Container, Card, Row, Col, Table} from 'react-bootstrap';
//components
import Home from '../components/Home';
import Login from '../components/Login';
class Sidebar extends Component {
  constructor(props){
    super(props);
    this.state = {
      showing : this.props.showing,
      logged: ''
    }
  }

  hides = () => {
    // this.setState({
    //   showing: "none"
    // });
  }

  componentWillReceiveProps(newProps){
    this.setState({
      showing: newProps.showing
    });
  }

  componentDidMount(){
    // Sidebar Toggle Menu Click
    
    $("#menu-toggle").click(function(e) {
      e.preventDefault();
      $("#wrapper").toggleClass("toggled");
     });
  }
  logout = () => {
    sessionStorage.setItem("userId","");
  }
render () {
  if (sessionStorage.getItem("userId") === ""){
      return (<div>
        <Link to="/"></Link>
      </div>)
  }
    return (
        <div class="d-flex" id="wrapper">
                <div class="bg-light border-right" id="sidebar-wrapper"  style={{display: this.state.showing}}>
                <div class="sidebar-heading"><b>Softtouch Water Billing</b></div>
                  <div class="list-group list-group-flush">
                    <Link to="/Home"><a href="#" class="list-group-item list-group-item-action bg-light">Home</a></Link>
                    <Link to="/Household"><a href="#" class="list-group-item list-group-item-action bg-light">Household Management</a></Link>
                    <Link to="/Billing"><a href="#" class="list-group-item list-group-item-action bg-light">Billing</a></Link>
                    <Link to="/Payment"><a href="#" class="list-group-item list-group-item-action bg-light">Payment</a></Link>
                    
                          <a style={{color:"black"}} class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          Reports
                          </a>
                          <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                            <Link to="/Soa"><a href="#" class="list-group-item list-group-item-action bg-light">Print SOA</a></Link>
                            <Link to="/Reports"><a class="dropdown-item" href="#">Paid/Unpaid Household, Daily/Monthly Collection</a></Link>
                          </div>
                  </div>
                </div>
                <div id="page-content-wrapper">
                  <nav class="navbar navbar-expand-lg navbar-light bg-light border-bottom" style={{backgroundColor: "#4d5054 !important", display: this.state.showing}}>
                    <button class="btn btn-primary" id="menu-toggle"><i class="icon-align-justify"></i></button>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                      <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                      <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
                        <li class="nav-item dropdown">
                          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Settings
                          </a>
                          <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                            <Link to="/Settings"><a href="#" class="list-group-item list-group-item-action bg-light">Settings(Expenses)</a></Link>
                            <Link to="/"> <a class="dropdown-item" href="#" onClick={this.logout}>Logout</a></Link>
                          </div>
                        </li>
                      </ul>
                    </div>
                  </nav>
                  <div class="container-fluid">
                    {this.props.children}
                  </div>
                </div>
            </div>
        
    );
}

}

export default Sidebar