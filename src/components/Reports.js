import React, {Component} from 'react'
import Sidebar from './Sidebar';
import Select from 'react-select';
import axios from 'axios';
import  {Button, Form, Container, Card, Row, Col, Table} from 'react-bootstrap';
import $ from 'jquery'; 
import * as XLSX from 'xlsx';
import Ip from './../Ip';
class Soa extends Component{
    constructor(props) {
        super(props);
        this.state = {
            type: "1",
            bill: "",
            monthincomeDisplay: '',
            dailyincomeDisplay: 'none',
            incomeType: 3,
            month: 1,
            year: 2021,
            daily: 1,
            errorMsg: ''
        }
    }

    getAllbilling = () => {
        axios.get('http://'+Ip.IP+'/billing/getAll')
        .then(response => {
            console.log(response.data)
            this.setState({
                billings: response.data
            });
        });
    }

    onChangebill = (e) => {
        this.setState({
            bill : e.value
        });
    }

    handleChange = (e) => {
        const name = e.target.name;
        this.setState({
            [name] : e.target.value
        });
    }
    
    onChangeIncomeType = (e) => {
        var params = new URLSearchParams();
        params.append("id", e.target.value);
        const name = e.target.name;
        this.setState({
            [name] : e.target.value
        });
        if (e.target.value === "2"){
            this.setState({
                monthincomeDisplay: 'none',
                dailyincomeDisplay: ''
            });
       } else {
        this.setState({
            monthincomeDisplay: '',
            dailyincomeDisplay: 'none'
        });
       }
    }

    onChangeType = (e) => {
        var params = new URLSearchParams();
        params.append("id", e.target.value);
       
        const name = e.target.name;
        this.setState({
            [name] : e.target.value
        });
        console.log(e.target.value);
    }

    download = (e) =>{
        e.preventDefault();
        if (this.state.bill === ''){
            this.setState({
                errorMsg: 'Required!'
            }, () => {
                setTimeout(
                    () => this.setState({ errorMsg: '' }), 
                    3000
                );
            });
        } else {
            var params = new URLSearchParams();
            params.append("bill_id", this.state.bill);
            params.append("stats", this.state.type);
            axios.post
            ('http://'+Ip.IP+'/billing/reportPaidStatusHousehold', params)
            .then(response => {
                var data = response.data.map((data)=>({
                    fullname :  data._name,
                    bill_no : data.bill_nos,
                    bill_coverage: data.bill_coverage,
                    amount_due: data.amount_due,
                    paid_amount: data.paid_amount
                }))
                const csvData = this.objectToCSV(data);
            });
        }
        
    }

    downloadCollection = (e) => {
        e.preventDefault();

            var params = new URLSearchParams();
            params.append("month", this.state.month);
            params.append("year", this.state.year);
            params.append("incomeType", this.state.incomeType);
            params.append("bill_id", this.state.bill);
            params.append("day", this.state.day);
            axios.post
            ('http://'+Ip.IP+'/billing/reportCollectionBymonth', params)
            .then(response => {
                var data = response.data.map((data)=>({
                    fullname :  data._name,
                    bill_no : data.bill_nos,
                    bill_coverage: data.bill_coverage,
                    amount_due: data.amount_due,
                    paid_amount: data.paid_amount
                }))
                const csvData = this.objectToCSV(data);
            });
    }

    objectToCSV = (data) => {
        const date = new Date();
        const datefile = date.getFullYear() + "" + (date.getMonth() + 1) + "" + date.getDate();
        var createXLSLFormatObj = [];
        var xlsHeader = ["fullname", "bill_no", "bill_coverage", "amount_due", "paid_amount"];
        createXLSLFormatObj.push(xlsHeader);
        $.each(data, function(index, value) {
            const csvRows = [];
            $.each(value, function(ind, val) {
                csvRows.push(val);
            });
            createXLSLFormatObj.push(csvRows);
        });
        console.log(createXLSLFormatObj);
        var wb = XLSX.utils.book_new(),
            ws = XLSX.utils.aoa_to_sheet(createXLSLFormatObj);

        /* Add worksheet to workbook */
        XLSX.utils.book_append_sheet(wb, ws, "ws_name");
        XLSX.writeFile(wb, "Households"+datefile+".xlsx");
    }


    downloadAsExcel = () => {
        
        axios.post
        ('http://'+Ip.IP+'/household/exportHousehold')
        .then(response => {
            var data = response.data.map((data)=>({
                id : data.id,
                fullname :  data.name + ' '+data.mname+ ' '+ data.lname,
                previous : data.prev,
                current: ''
            }))
            const csvData = this.objectToCSV(data);
        });
        
    }


    componentDidMount(){
        this.getAllbilling();
    }

    render () {
        return (
            <div>
                <Sidebar>
                    
                    <h1>Reports</h1>
                    <Form onSubmit= {(e)=>{
                        this.download(e);
                    }}>
                        <Row>
                            <Col md="3" style={{display: this.state.display}}>
                                    <Select placeholder = "Select Bill" onChange={(e) => {
                                this.onChangebill(e)}}
                                    options={this.state.billings}
                                />
                                <b style={{color: "#b30000"}}>{this.state.errorMsg}</b>
                            </Col>
                            <Col md="3">
                            </Col>
                            <Col md="3"></Col>
                        </Row>
                        <Row style={{marginTop: "10px"}}>
                            <Col md="3" style={{display: this.state.display}}>
                                    <select name = "type" onChange = {(e)=>{
                                        this.onChangeType(e);
                                    }} class="form-control">
                                        <option value="1">Paid Household(s)</option>
                                        <option value="0">Unpaid Household(s)</option>
                                    </select>
                            </Col>
                            <Col md="3">
                                <Button type="submit">Download Paid/Unpaid Household</Button>
                            </Col>
                            <Col md="3"></Col>
                        </Row>
                    </Form>
                    <Form onSubmit={(e) => {
                        this.downloadCollection(e);
                    }}>
                        <Row style={{marginTop: "10px"}}>
                                <Col md="3" style={{display: this.state.display}}>
                                        <select name = "incomeType" onChange = {(e)=>{
                                            this.onChangeIncomeType(e);
                                        }} class="form-control">
                                            <option value="2" >Daily Income</option>
                                            <option value="3" selected>Monthly Income</option>
                                        </select>
                                </Col>
                                <Col md="3">
                                    <Row>
                                        
                                        <Col md = "5">
                                        <select name = "year" onChange = {(e)=>{
                                            this.handleChange(e);
                                        }} class="form-control">
                                            <option value="">Year</option>
                                            <option value="2019">2018</option>
                                            <option value="2019">2019</option>
                                            <option value="2020">2020</option>
                                            <option value="2021" selected>2021</option>
                                            <option value="2022">2022</option>
                                        </select></Col>
                                        <Col md = "5"><select  name = "month" onChange = {(e)=>{
                                            this.handleChange(e);
                                        }} class="form-control">
                                            <option value="1">January</option>
                                            <option value="2">February</option>
                                            <option value="3">March</option>
                                            <option value="4">April</option>
                                            <option value="5">May</option>
                                            <option value="6">June</option>
                                            <option value="7">July</option>
                                            <option value="8">August</option>
                                            <option value="9">September</option>
                                            <option value="10">October</option>
                                            <option value="11">November</option>
                                            <option value="12">December</option>
                                        </select></Col>
                                        <Col md="2" style={{display: this.state.dailyincomeDisplay}}><select  name = "day" onChange = {(e)=>{
                                            this.handleChange(e);
                                        }} class="form-control" value={this.state.day}>
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                            <option value="5">5</option>
                                            <option value="6">6</option>
                                            <option value="7">7</option>
                                            <option value="8">8</option>
                                            <option value="9">9</option>
                                            <option value="10">10</option>
                                            <option value="11">11</option>
                                            <option value="12">12</option>
                                            <option value="13">13</option>
                                            <option value="14">14</option>
                                            <option value="15">15</option>
                                            <option value="16">16</option>
                                            <option value="17">17</option>
                                            <option value="18">18</option>
                                            <option value="19">19</option>
                                            <option value="20">20</option>
                                            <option value="21">21</option>
                                            <option value="22">22</option>
                                            <option value="22">22</option>
                                            <option value="22">22</option>
                                            <option value="22">22</option>
                                            <option value="23">23</option>
                                            <option value="24">24</option>
                                            <option value="25">25</option>
                                            <option value="26">26</option>
                                            <option value="26">26</option>
                                            <option value="27">27</option>
                                            <option value="28">28</option>
                                            <option value="29">29</option>
                                            <option value="30">30</option>
                                            <option value="31">31</option>
                                        </select></Col>
                                    </Row>
                                        
                                        
                                     
                                         
                                </Col>
                                
                                <Col md="3"><Button type="submit">Download Daily/Monthly Income</Button></Col>  
                            </Row>
                    </Form>
                </Sidebar>
            </div>
        )
    }

}

export default Soa