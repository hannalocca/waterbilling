import React, {Component, useState} from 'react';
import {Button, Form, Container, Card, Row, Col, Table} from 'react-bootstrap';
import XLSX from 'xlsx';
import axios from 'axios';
import Swal from 'sweetalert2'
import {Link} from 'react-router-dom';
import CurrencyFormat from 'react-currency-format'

//components
import Sidebar from './Sidebar';
import Ip from './../Ip';

class Billing extends Component{
    constructor(props){
        super(props);
        this.state = {
            items : [],
            mainWatercurrent: 0,
            mainWaterprevious: 0,
            mainEleccurrent: 0,
            mainElecprevious: 0,
            totalConsumption: 0,
            totalkwConsumed: 0,
            electricityBillPeso: 0,
            electricityBillkw:0,
            electricityPriceKw:0,
            totalelectricityBillPeso: 0,
            rateCubic: 0,
            expense_lists: [],
            totalExpenses: 0,
            bill_coveragefrom: '',
            bill_coverageto: '',
            presentReadingDate: '',
            previousReadingDate: '',
            serviceCharge: 0,
            secondBracket: 0,
            thirdBracket: 0,
            fourthBracket: 0,
            succeedingRate: 0,
            noofHousehold: 193,
            mcwdtotalWaterConsumption: 0,
            _1st: 137.86,
            _2nd: 16.8,
            _3rd: 19.77,
            _4th: 48.4,
            totalserviceCharges: 0,
            totalsecondBrackets: 0,
            totalthirdBrackets: 0,
            totalfourthBrackets:0,
            totalExcess: 0,
            totalMCWD: 0,
            _101: 10,
            _102: 10,
            _103: 10,
            _20: 20,
            excess_rate: 0
        }
    }

    handleChange = (element) =>{
        const name = element.target.name;
        this.setState({
            [name] : element.target.value
        });
    }
    
    computeBreakdown = (e) =>{
        e.preventDefault();
        
        this.setState({
            totalConsumption : this.state.mainWatercurrent - this.state.mainWaterprevious,
            totalkwConsumed: this.state.mainEleccurrent - this.state.mainElecprevious,
            electricityPriceKw: this.state.electricityBillPeso / this.state.electricityBillkw
        },
        ()=> {
            this.computeMCWD();
            this.setState({
                totalelectricityBillPeso: this.state.totalkwConsumed * parseFloat((this.state.electricityPriceKw).toFixed(2))
            }, () =>{
                this.setState({
                    rateCubic: (parseFloat(this.state.totalelectricityBillPeso) + parseFloat(this.state.totalExpenses) + parseFloat(this.state.totalMCWD)) / this.state.totalConsumption
                }, () => {
                    
                });
            });
        }); 
        
    }
    
    saveBill = (e) => {
        e.preventDefault();
        var dt = new Date();
        var due_date = new Date(this.state.bill_coverageto);
        due_date.setDate(due_date.getDate()+12);
            Swal.fire({
                title: 'Do you want to save the changes?',
                showDenyButton: true,
                showCancelButton: true,
                confirmButtonText: `Save`,
                denyButtonText: `Don't save`,
                }).then((result) => {
                /* Read more about isConfirmed, isDenied below */
                    if (result.value) {
                        const form = document.getElementById('billForm');
                        var params = new URLSearchParams();
                        params.append("mainWaterprevious", form.mainWaterprevious.value);
                        params.append("mainWatercurrent", form.mainWatercurrent.value);
                        params.append("mainElecprevious", form.mainElecprevious.value);
                        params.append("mainEleccurrent", form.mainEleccurrent.value);
                        params.append("electric_bill", form.electricityBillPeso.value);
                        params.append("kw", this.state.electricityBillkw);
                        params.append("total_consumption", this.state.totalConsumption);
                        params.append("total_kw_consumed", this.state.totalkwConsumed);
                        params.append("price_kw", this.state.electricityPriceKw);
                        params.append("total_cost_of_water", (parseFloat(this.state.totalMCWD) + parseFloat(this.state.totalelectricityBillPeso) + parseFloat(this.state.totalExpenses)));
                        params.append("total_electric_bill_peso", this.state.totalelectricityBillPeso);
                        params.append("bill_coverage", this.state.bill_coveragefrom + ' - ' +this.state.bill_coverageto);
                        params.append("present_reading_date", this.state.presentReadingDate);
                        params.append("previous_reading_date", this.state.previousReadingDate);
                        params.append("bill_no", "W"+dt.getFullYear() + "-" + (dt.getMonth() + 1)+"00");
                        params.append("expense_lists", JSON.stringify(this.state.expense_lists));
                        params.append("rateCubic", this.state.rateCubic);
                        params.append("mcwd", this.state.totalMCWD);
                        params.append("due_date", (due_date.getMonth()+1)+"/"+due_date.getDate()+"/"+due_date.getFullYear());
                        axios.post
                        ('http://'+Ip.IP+'/billing/save', params)
                        .then(response =>{ 
                                this.props.history.push('/Billing');
                                Swal.fire("Saved!", '', 'success');
                            });
                    } else{
                        Swal.fire('Changes are not saved', '', 'info')
                    }
                })

    }

    getSettings = () =>{
        axios.get('http://'+Ip.IP+'/settings/getLists')
        .then(response => 
            this.setState({expense_lists: response.data},() =>{
                var sum = response.data.reduce((total, currentValue) => total = total + parseInt(currentValue.amount),0);
                this.setState({
                    totalExpenses: sum
                })
            }));
            
    }

    computeMCWD = () => {
        //consumption
        var _consumption1 = parseFloat(this.state.noofHousehold) * parseFloat(this.state._101);
        var _consumption2 = parseFloat(this.state.noofHousehold) * parseFloat(this.state._102);
        var _consumption3 = parseFloat(this.state.noofHousehold) * parseFloat(this.state._103);
        var _consumption4 = parseFloat(this.state.noofHousehold) * parseFloat(this.state._20);
        
        
        
        //total
        var totalserviceCharge = parseFloat(this.state.noofHousehold) * parseFloat(this.state._1st);
        var totalsecondBracket = (parseFloat(_consumption2) * parseFloat(this.state._2nd));
        var totalthirdBracket = (parseFloat(_consumption3) * parseFloat(this.state._3rd));
        var totalfourthBracket = (parseFloat(_consumption4) * parseFloat(this.state._4th));
        var totalsucceedingRate = parseFloat(totalserviceCharge) + parseFloat(totalsecondBracket) + parseFloat(totalthirdBracket) + parseFloat(totalfourthBracket);
        var excess = parseFloat(this.state.totalConsumption) - parseFloat(_consumption1 + _consumption2 + _consumption3 + _consumption4);
        var totalExces = parseFloat(excess) * parseFloat(this.state.excess_rate);
        
        var totalsucceedingRate = parseFloat(totalserviceCharge) + parseFloat(totalsecondBracket) + parseFloat(totalthirdBracket) + parseFloat(totalfourthBracket) + parseFloat(totalExces);
        this.setState({
            totalserviceCharges: totalserviceCharge,
            totalsecondBrackets: totalsecondBracket,
            totalthirdBrackets: totalthirdBracket,
            totalfourthBrackets: totalfourthBracket,
            serviceCharge: _consumption1,
            secondBracket: _consumption2,
            thirdBracket: _consumption3,
            fourthBracket: _consumption4,
            mcwdtotalWaterConsumption: (_consumption1 + _consumption2 + _consumption3 + _consumption4),
            totalExcess: totalExces,
            totalMCWD: totalsucceedingRate
        });
    }

    componentDidMount(){
        this.getSettings();
        this.computeMCWD();
    }

    render (){
        // if(this.state.items.length === 0){
            
            
        //     return <div><Form.Control type="file" onChange={(e)=> {
        //         const file = e.target.files[0];
        //         this.readExcel(file);
        //     }}></Form.Control></div>;
        // }
        return (
            <div>
                <Sidebar>
                    <Container style={{marginTop:'10px'}}>
                        <Row>
                            <Card style={{minWidth: '100%'}}>
                                <Card.Header style={{fontWeight:"bold", fontSize: "40px"}}>
                                <Row>
                                    <Col md="6">Create Bill</Col>
                                    <Col md="5"></Col>
                                    <Col md="1"><Link to="/Billing"><Button variant="default"><b>X</b></Button></Link></Col>
                                </Row>
                                </Card.Header>
                            </Card>
                            <Form id="billForm" style={{width: "100%"}} onSubmit = {(e) => {
                                this.computeBreakdown(e);
                            }}>        
                                <Card style={{minWidth: '100%', marginTop: '10px'}}>
                                    <Card.Header style={{fontWeight:"bold", fontSize: "25px"}}>
                                        Billing breakdown
                                    </Card.Header>
                                </Card>  
                                <table class="table table-bordered" style={{backgroundColor: "#eeeded"}}>
                                <tr>
                                        <td>Bill Coverage</td>
                                        <td><Form.Control type="date" required name = "bill_coveragefrom" placeholder="From" onChange = {(e) =>{
                                            this.handleChange(e);
                                        }}></Form.Control> - <Form.Control required type="date" name = "bill_coverageto" placeholder="to" onChange = {(e) =>{
                                            this.handleChange(e);
                                        }}></Form.Control></td>
                                        <td class="current_meter" style={{width: "230px"}}>
                                            <label style={{display: 'block'}}>Present Reading Date:</label>
                                            <label style={{display: 'block', marginTop: '50px'}}>Previous Reading Date:</label>
                                        </td>
                                            <td  colspan="5" style={{textAlign: "center"}}><Form.Control required type="date" name = "presentReadingDate" placeholder="Present Reading Date" onChange = {(e) =>{
                                            this.handleChange(e);
                                        }}></Form.Control> - <Form.Control required type="date" name = "previousReadingDate" placeholder="Previous Reading Date" onChange = {(e) =>{
                                            this.handleChange(e);
                                        }}></Form.Control></td>
                                    </tr>
                                    <tr>
                                        <td>Main water meter</td>
                                        <td><Form.Control type="text" required name = "mainWatercurrent" placeholder="Current reading" onChange = {(e) =>{
                                            this.handleChange(e);
                                        }}></Form.Control></td>
                                        <td class="current_meter"><Form.Control required name = "mainWaterprevious" type="text" placeholder="Previous reading" onChange={(e) =>{
                                            this.handleChange(e);
                                        }}></Form.Control></td>
                                            <td style={{textAlign: "center"}} colspan="5"><b>Total Consumption:</b> {this.state.totalConsumption} </td>
                                    </tr>
                                    <tr>
                                        <td>No. of household</td>
                                        <td><Form.Control required name = "noofHousehold" value = {this.state.noofHousehold} type="text" placeholder="Enter No. of household" onChange={(e) =>{
                                            this.handleChange(e);
                                        }}></Form.Control></td>
                                        <td colspan="6"></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td>Total water consumption</td>
                                        <td>Standard Cubic</td>
                                        <td># of Household</td>
                                        <td>Rate</td>
                                        <td>Total</td>
                                    </tr>
                                    <tr>
                                        <td>Service Charge</td>
                                        <td>First 10 cm3</td>
                                        <td>({this.state.noofHousehold} x <input type="text" onChange={(e) =>{
                                            this.handleChange(e);
                                        }} name="_101" style={{width: "70px"}} value={this.state._101}></input>)</td>
                                        <td>{this.state.serviceCharge}</td>
                                        <td>none</td>
                                        <td>{this.state.noofHousehold}</td>
                                        <td><Form.Control required name = "_1st" value = {this.state._1st} type="text" onChange={(e) =>{
                                            this.handleChange(e);
                                        }}></Form.Control></td>
                                        <td><CurrencyFormat value={(this.state.totalserviceCharges).toFixed(2)} displayType={'text'} thousandSeparator={true} /></td>
                                    </tr>
                                    <tr>
                                        <td>Second Bracket</td>
                                        <td>11 cm3 to 20 cm3</td>
                                        <td>({this.state.noofHousehold} x <input type="text" onChange={(e) =>{
                                            this.handleChange(e);
                                        }} style={{width: "70px"}} name="_102" value={this.state._102}></input>)</td>
                                        <td>{this.state.secondBracket}</td>
                                        <td>10</td>
                                        <td>{this.state.noofHousehold}</td>
                                        <td><Form.Control required name = "_2nd" value = {this.state._2nd} type="text" onChange={(e) =>{
                                            this.handleChange(e);
                                        }}></Form.Control></td>
                                        <td><CurrencyFormat value={(this.state.totalsecondBrackets).toFixed(2)} displayType={'text'} thousandSeparator={true} /></td>
                                    </tr>
                                    <tr>
                                        <td>Third Bracket</td>
                                        <td>21 cm3 to 30 cm3</td>
                                        <td>({this.state.noofHousehold} x <input type="text" onChange={(e) =>{
                                            this.handleChange(e);
                                        }} style={{width: "70px"}} name="_103" value={this.state._103}></input>)</td>
                                        <td>{this.state.thirdBracket}</td>
                                        <td>10</td>
                                    <td>{this.state.noofHousehold}</td>
                                        <td><Form.Control required name = "_3rd" value = {this.state._3rd} type="text" onChange={(e) =>{
                                            this.handleChange(e);
                                        }}></Form.Control></td>
                                        <td><CurrencyFormat value={(this.state.totalthirdBrackets).toFixed(2)} displayType={'text'} thousandSeparator={true} /></td>
                                    </tr>
                                    <tr>
                                        <td>Fourth Bracket</td>
                                        <td>31 cm3 to 50cm3</td>
                                        <td>({this.state.noofHousehold} x <input type="text" onChange={(e) =>{
                                            this.handleChange(e);
                                        }} style={{width: "70px"}} name="_20" value={this.state._20}></input>)</td>
                                        <td>{this.state.fourthBracket}</td>
                                        <td>20</td>
                                        <td>{this.state.noofHousehold}</td>
                                        <td><Form.Control required name = "_4th" value = {this.state._4th} type="text" onChange={(e) =>{
                                            this.handleChange(e);
                                        }}></Form.Control></td>
                                        <td><CurrencyFormat value={(this.state.totalfourthBrackets).toFixed(2)} displayType={'text'} thousandSeparator={true} /></td>
                                    </tr>
                                    <tr>
                                        <td>Succeeding Rate</td>
                                        <td>exceess</td>
                                        <td></td>
                                        <td>{this.state.mcwdtotalWaterConsumption}</td>
                                        <td colspan="2">({this.state.totalConsumption} - {this.state.mcwdtotalWaterConsumption}) x <input type="text" onChange={(e) =>{
                                            this.handleChange(e);
                                        }} name="excess_rate" style={{width: "50px"}} value={this.state.excess_rate}></input></td>
                                        <td></td>
                                        <td><CurrencyFormat value={(this.state.totalExcess).toFixed(2)} displayType={'text'} thousandSeparator={true} /></td>
                                    </tr>
                                    <tr>
                                        <td>Total Cubic Meter</td>
                                        <td></td>
                                        <td></td>
                                        <td>{this.state.totalConsumption}</td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td><b><CurrencyFormat value={(this.state.totalMCWD).toFixed(2)} displayType={'text'} thousandSeparator={true} /></b></td>
                                    </tr>
                                </table>
                                <table class="table table-bordered" style={{backgroundColor: "#eeeded"}}>
                                    
                                    <tr>
                                        <td>Main Electricity meter:</td>
                                        <td><Form.Control type="text" required name="mainEleccurrent" onChange = {(e)=>{
                                            this.handleChange(e);
                                        }} placeholder="Current reading"></Form.Control></td>
                                        <td class="previous_meter"><Form.Control required type="text" name = "mainElecprevious" onChange = {(e) => {
                                            this.handleChange(e);
                                        }}placeholder="Previous reading"></Form.Control></td>
                                        <td style={{textAlign: "center"}}><b>Total (kw) consumed:</b> {this.state.totalkwConsumed}</td>
                                    </tr>
                                    <tr>
                                        <td>Electricity Bill (VECO):</td>
                                        <td><Form.Control type="text" required name = "electricityBillPeso" placeholder="Peso" onChange = {(e) => {
                                            this.handleChange(e);
                                        }}></Form.Control></td>
                                        <td class="previous_meter"><Form.Control required name = "electricityBillkw" type="text" placeholder="kw" onChange = {(e) => {
                                            this.handleChange(e);
                                        }}></Form.Control></td>
                                        <td style={{textAlign: "center"}}><b>Price/kw:</b> <CurrencyFormat value={(this.state.electricityPriceKw).toFixed(2)} displayType={'text'} thousandSeparator={true} /></td>
                                    </tr>
                                    <tr>
                                        <td>Computations:</td>
                                        <td colspan="2"></td>
                                        <td><Button type="submit">Compute</Button></td>
                                        
                                    </tr>
                                    <tr>
                                        <td rowspan="3"></td>
                                        <td>Total KW used</td>
                                        <td>{this.state.totalkwConsumed}</td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>* Rate per kw</td>
                                        <td><CurrencyFormat value={(this.state.electricityPriceKw).toFixed(2)} displayType={'text'} thousandSeparator={true} /></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td><b>Total Electric bill (peso)</b></td>
                                        <td><b><CurrencyFormat value={(this.state.totalelectricityBillPeso).toFixed(2)} displayType={'text'} thousandSeparator={true} /></b></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td rowspan={1 + this.state.expense_lists.length}>Add:</td>
                                        <td>Total Cubic meter</td>
                                        <td><CurrencyFormat value={(this.state.totalMCWD).toFixed(2)} displayType={'text'} thousandSeparator={true} /></td>
                                        <td></td>
                                    </tr>
                                    {this.state.expense_lists.map((data) => (
                                        <tr key={data.id}>
                                            <td>{data.expense_name}</td>
                                            <td><CurrencyFormat value={data.amount} displayType={'text'} thousandSeparator={true} /></td>
                                            <td></td>
                                        </tr>
                                    ))}
                                    <tr>
                                        <td></td>
                                        <td><b>Total Cost of water:</b></td>
                                        <td><b><CurrencyFormat value={(parseFloat(this.state.totalMCWD) + parseFloat(this.state.totalelectricityBillPeso) + parseFloat(this.state.totalExpenses)).toFixed(2)} displayType={'text'} thousandSeparator={true} /></b></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>Divided by:</td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td rowspan="3"></td>
                                        <td>Total water consumption</td>
                                        <td>{this.state.totalConsumption}</td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td><b>Price of water (m3)</b></td>
                                        <td><b>{(this.state.rateCubic).toFixed(2)}</b></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td><Button variant="success" onClick = {(e)=> {
                                            this.saveBill(e);
                                        }}>Save</Button></td>
                                    </tr>
                                    
                                </table>
                            </Form>  
                        </Row>
                    </Container>
                </Sidebar>
            </div>
        )
    }
}

export default Billing