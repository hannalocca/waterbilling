import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import {Button, Form, Container, Card, Row, Col, Table, Modal} from 'react-bootstrap';
import * as XLSX from 'xlsx';
import {Link, Redirect, BrowserRouter as Router, Route} from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'jquery/dist/jquery.min.js';
//Datatable Modules
import "datatables.net-dt/js/dataTables.dataTables"
import "datatables.net-dt/css/jquery.dataTables.min.css"
import $ from 'jquery'; 
import axios from 'axios';
import { useHistory } from "react-router-dom";
import Swal from 'sweetalert2'
import Ip from './../Ip';

//components
import Sidebar from './Sidebar';
import Settings from './Settings';
class Household extends Component{
    constructor(props){
        super(props);
        this.state = {
            household_lists:[],
            modal: false
        }
    }
    downloadAsExcel = (id) => {
        // const data = {consumption: '10.11',
        //     current: '110',
        //     fullname: "Gerald Serenio",
        //     id: '11',
        //     previous: '100'}
        // const worksheet = XLSX.utils.json_to_sheet(data);
        // const workbook = {
        //     Sheets: {
        //         'data': worksheet
        //     },
        //     Sheetnames:['household']
        // };

        // const excelBuffer = XLSX.write (workbook, {bookType: 'xlsx', type: 'array'});
        // console.log(excelBuffer);
        // // const datas = new Blob([buffer], {type: EXCEL_TYPE});

    }

    billHousehold = (id) => {
        this.props.history.push({pathname: '/BillHousehold',
        state: {id: id, title:'BillHousehold'}});
    }

    viewBilling = (id) => {
        this.props.history.push({pathname: '/ViewBilling',
        state: {id: id, title:'View'}});
    }

    
    delete = (id) => {
        Swal.fire({
            title: 'Do you want to save the changes?',
            showDenyButton: true,
            showCancelButton: true,
            confirmButtonText: `Save`,
            denyButtonText: `Don't save`,
            }).then((result) => {
                console.log(result.value);
            /* Read more about isConfirmed, isDenied below */
                if (result.value) {
                    var params = new URLSearchParams();
                    params.append("id", id);
                    axios.post
                    ('http://'+Ip.IP+'/billing/delete', params)
                    .then(response => {
                        
                        $('#example').DataTable().clear().destroy();
                        this.getAllbilling()
                    });

                        
                    Swal.fire('Saved!', '', 'success')
                } else{
                    Swal.fire('Changes are not saved', '', 'info')
                }
            })
      
    }
    getAllbilling = () => {
        axios.get('http://'+Ip.IP+'/billing/getAll')
        .then(response => 
                this.setState({
                    household_lists: response.data
                }, () =>{
                    console.log(response);
                    $(document).ready(function () {
                        $('#example').DataTable();
                    });
                    
                    this.table = $('#example').DataTable( {
                         retrieve: true,
                         data: response.data,
                         columns: [
                            { data: 'bill_nos'},
                            { data: 'date_created'},        
                            { data: 'bill_coverage'},   
                            { data: 'action'},
                           ],
                        columnDefs: [
                            {
                            targets: [3],
                            width: 180,
                            className: "center",
                            createdCell: (td, cellData, rowData) =>
                            ReactDOM.render(
                                <div>
                                    <button
                                    id={rowData.id}
                                    class="btn btn-primary"
                                    onClick={() => {
                                    this.viewBilling(rowData.action);
                                    }}
                                    > <i class="icon-fixed-width icon-eye-open"></i> </button>
                                    <button
                                    id={rowData.id}
                                    class="btn btn-info"
                                    onClick={(e) => {
                                    this.billHousehold(rowData.action);
                                    }}
                                    > <i class="icon-fixed-width icon-pencil"></i> </button>
                                    <button
                                    id={rowData.id}
                                    class="btn btn-danger"
                                    onClick={() => {
                                        this.delete(rowData.action);
                                    }}
                                    > <i class="icon-fixed-width icon-trash"></i> </button>
                                </div>
                                 , td ), },]
                            }); 
                        $('#example_wrapper').css( 'width', '100%' );
                        $('#example_wrapper').css( 'background-color', 'white');
                        $('#example_wrapper').css('padding', '5px');
                        $('#example_wrapper').css('padding-top', '20px');
                        $('#example').css( 'width', '100%');
                        this.table.columns.adjust().draw();
                    
                })
            );
    }
    

    componentDidMount() {
        //initialize datatable
        
        
        this.getAllbilling();

     }
     
    render(){
        return (
            <div>
                <Sidebar>
                        
                    <Container>
                        <Row style={{marginTop: "5px"}}>
                            <Col md ="1"></Col>
                            <Col md ="5"></Col>
                            <Col md ="3"></Col>
                            
                            <Col md ="1"></Col>
                            <Col md ="2"><Link to={{pathname: "/CreateBill", state:{id: '', title: 'Add'}}}><Button style={{marginTop:'50px',marginLeft: '117px'}} variant="primary" onClick={this.downloadAsExcel}><i class="icon-fixed-width icon-plus"></i></Button></Link></Col>
                            
                        </Row>
                        <Row style={{marginTop: "10px"}}>
                        <Card style={{width:"100%", backgroundColor: "white"}}>
                                <Card.Header><div style={{padding:'5px'}}><h1>Billing(s) Management</h1></div></Card.Header>
                            </Card>
                            
                            <table id="example" class="display">
                                <thead>
                                    <tr>
                                        <th>Bill No.</th>
                                        <th>Bill Date</th>
                                        <th>Bill Coverage</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    
                                </tbody>
                            </table>
                        </Row>
                    </Container>
                </Sidebar>
            </div>
        )
    }
}

export default Household