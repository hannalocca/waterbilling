import React, {Component} from 'react'
import  {Button, Form, Container, Card, Row, Col, Table} from 'react-bootstrap';
import axios from 'axios';
import {Link} from 'react-router-dom';
import Swal from 'sweetalert2'

import Ip from './../Ip';
//
import Sidebar from './Sidebar';
class AddHousehold extends Component{
    constructor(props){
        super(props);
        this.state = {
            name: '',
            mname: '',
            lname: '',
            lot: '',
            block: '',
            phase: '',
            meter_num: '',
            stub_num: '',
            meterflg: '0',
            color: '',
            subdivision: '',
            phone_number: ''
        }
    }

    handleChange = (element) => {
        this.setState({
            [element.target.name]: element.target.value
        });
    }

    changemeterFlg = () => {
        this.setState({
            meterflg: '1'
        });
    }

    saveHousehold = (e) =>{
        e.preventDefault();
        Swal.fire({
            title: 'Do you want to save the changes?',
            showDenyButton: true,
            showCancelButton: true,
            confirmButtonText: `Save`,
            denyButtonText: `Don't save`,
            }).then((result) => {
                console.log(result.value);
            /* Read more about isConfirmed, isDenied below */
                if (result.value) {
                    const form = document.getElementById('householdForm');
                    var params = new URLSearchParams();
                    params.append("id", this.props.location.state.id);
                    params.append("name", form.name.value);
                    params.append("mname", form.mname.value);
                    params.append("lname", form.lname.value);
                    params.append("lot", form.lot.value);
                    params.append("block", form.block.value);
                    params.append("phase", form.phase.value);
                    params.append("meter_num", form.meter_num.value);
                    params.append("stub_num", form.stub_num.value);
                    params.append("meterflg", this.state.meterflg);
                    params.append("subdivision", this.state.subdivision);
                    params.append("phone_number", this.state.phone_number);
                    axios.post
                    ('http://'+Ip.IP+'/household/save', params)
                    .then(response =>{ 
                            this.setState({
                            response: response.data.msg,
                            color: response.data.color
                        })
                        if (response.data.color === 'red'){
                            Swal.fire(response.data.msg, '', 'info')
                        } else {
                            Swal.fire( this.props.location.state.id === '' ? response.data.msg +" "+ this.props.location.state.title +'ed!' : response.data.msg +" "+ this.props.location.state.title +'d!', '', 'success')
                            if (this.props.location.state.id !== ''){
                                this.props.history.push('/Household');
                            } else {
                                this.setState({
                                    name: '',
                                    mname: '',
                                    lname: '',
                                    lot: '',
                                    block: '',
                                    phase: '',
                                    meter_num: '',
                                    stub_num: '',
                                    meterflg: '0',
                                    color: ''
                                });
                            }
                        }
                    });
                    
                    
                    
                } else{
                    Swal.fire('Changes are not saved', '', 'info')
                }
            })
        
    }

    getSettings = () =>{
        var params = new URLSearchParams();
        params.append("id", this.props.location.state.id);
        axios.post('http://'+Ip.IP+'/household/get', params)
        .then(response => 
            this.setState({
                name: response.data[0].name,
                mname: response.data[0].mname,
                lname: response.data[0].lname,
                lot: response.data[0].lot,
                block: response.data[0].block,
                phase: response.data[0].phase,
                meter_num: response.data[0].meter_num,
                stub_num: response.data[0].stub_num,
                subdivision: response.data[0].subdivision,
                phone_number: response.data[0].phone_number
            }));
    }

    componentDidMount(){
        if (this.props.location.state.id != '') {
            this.getSettings();
        }
    }

    render(){
        return(
            <div>
                <Sidebar>
                    <div className="container">
                        <div className="row">
                                <Card style={{marginTop:"100px", width:"100%"}}>
                                                <Card.Header style={{fontSize: "40px", fontWeight: "bold"}}>
                                                    <Row>
                                                        <Col md="6">{this.props.location.state.title} Household Form</Col>
                                                        <Col md="5"></Col>
                                                        <Col md="1"><Link to="/Household"><Button variant="default"><b>X</b></Button></Link></Col>
                                                    </Row>
                                                    
                                                </Card.Header>
                                                    <Row>
                                                        <Col md="5"></Col>
                                                        <Col md="2"></Col>
                                                        <Col md="5"></Col>
                                                    </Row>
                                                <Form id = "householdForm" style={{padding: "10px"}} onSubmit={(e) => {
                                                    this.saveHousehold(e);
                                                }}>
                                                    
                                                    <Row>
                                                        <Col md="2">
                                                        <Form.Label>
                                                            <b>First name:</b>
                                                        </Form.Label>
                                                        </Col>
                                                        <Col md="4">
                                                        <Form.Control type="text" placeholder="Enter Firstname" value={this.state.name} required name="name" onChange={(e) => {
                                                            this.handleChange(e);
                                                        }}></Form.Control>
                                                        </Col>
                                                        <Col md="2"><b>Block:</b></Col>
                                                        <Col md="4"><Form.Control type="text" placeholder="Enter Block" value={this.state.block} required name="block" onChange={(e) => {
                                                            this.handleChange(e);
                                                        }}></Form.Control></Col>
                                                    </Row>
                                                    <Row style={{marginTop:'10px'}}>
                                                        <Col md="2">
                                                        <Form.Label>
                                                            <b>Middle name:</b>
                                                        </Form.Label>
                                                        </Col>
                                                        <Col md="4">
                                                        <Form.Control type="text" placeholder="Enter Middlename" value={this.state.mname} required name="mname" onChange={(e) => {
                                                            this.handleChange(e);
                                                        }}></Form.Control>
                                                        </Col>
                                                        <Col md="2"><b>Lot:</b></Col>
                                                        <Col md="4"><Form.Control type="text" placeholder="Enter Lot" value={this.state.lot} required name="lot" onChange={(e) => {
                                                            this.handleChange(e);
                                                        }}></Form.Control></Col>
                                                    </Row>
                                                    <Row  style={{marginTop:'10px'}}>
                                                        <Col md="2">
                                                        <Form.Label>
                                                            <b>Last name:</b>
                                                        </Form.Label>
                                                        </Col>
                                                        <Col md="4">
                                                        <Form.Control type="text" placeholder="Enter Lastname" required value={this.state.lname} name="lname" onChange={(e) => {
                                                            this.handleChange(e);
                                                        }}></Form.Control>
                                                        </Col>
                                                        <Col md="2"><b>Phase:</b></Col>
                                                        <Col md="4"><Form.Control type="text" placeholder="Enter Phase" required value={this.state.phase} name="phase" onChange={(e) => {
                                                            this.handleChange(e);
                                                        }}></Form.Control></Col>
                                                    </Row>
                                                    <Row  style={{marginTop:'10px'}}>
                                                        <Col md="2">
                                                        <Form.Label>
                                                            <b>Meter no.:</b>
                                                        </Form.Label>
                                                        </Col>
                                                        <Col md="4">
                                                        <Form.Control type="text" placeholder="Enter Meter no." required name="meter_num" value={this.state.meter_num} onChange={(e) => {
                                                            this.handleChange(e);
                                                            this.changemeterFlg();
                                                        }}></Form.Control>
                                                        </Col>
                                                        <Col md="2"><b>Stub out no.:</b></Col>
                                                        <Col md="4"><Form.Control type="text" placeholder="Enter Stub out no." value={this.state.stub_num} required name="stub_num" onChange={(e) => {
                                                            this.handleChange(e);
                                                        }}></Form.Control></Col>
                                                    </Row>
                                                    <Row  style={{marginTop:'10px'}}>
                                                        <Col md="2">
                                                        <Form.Label>
                                                            <b>Phone no.:</b>
                                                        </Form.Label>
                                                        </Col>
                                                        <Col md="4">
                                                        <Form.Control type="text" placeholder="Enter Phone no." required name="phone_number" value={this.state.phone_number} onChange={(e) => {
                                                            this.handleChange(e);
                                                        }}></Form.Control>
                                                        </Col>
                                                        <Col md="2"><b>Subdivision:</b></Col>
                                                        <Col md="4"><Form.Control type="text" placeholder="Enter Subdivision" value={this.state.subdivision} required name="subdivision" onChange={(e) => {
                                                            this.handleChange(e);
                                                        }}></Form.Control></Col>
                                                    </Row>
                                                    <Row  style={{marginTop:'30px'}}>
                                                        <Col md="2">
                                                        </Col>
                                                        <Col md="4">
                                                        </Col>
                                                        <Col md="2"></Col>
                                                        <Col md="4"><Button type="submit" variant="success">{this.props.location.state.title}</Button></Col>
                                                    </Row>
                                                </Form>
                                        </Card>
                                        
                        </div>
                    </div>
                </Sidebar>
            </div>
        )
    }
}

export default AddHousehold