import React, {Component, useState} from 'react';
import {Button, Form, Container, Card, Row, Col, Table} from 'react-bootstrap';
import XLSX from 'xlsx';
import axios from 'axios';
import CurrencyFormat from 'react-currency-format'
import {Link} from 'react-router-dom';
import Ip from './../Ip';

//components
import Sidebar from './Sidebar';

class Billing extends Component{
    constructor(props){
        super(props);
        this.state = {
            items : [],
            mainWatercurrent: 0,
            mainWaterprevious: 0,
            mainEleccurrent: 0,
            mainElecprevious: 0,
            totalConsumption: 0,
            totalkwConsumed: 0,
            electricityBillPeso: 0,
            electricityBillkw:0,
            electricityPriceKw:0,
            totalelectricityBillPeso: 0,
            rateCubic: 0,
            expense_lists: [],
            totalExpenses: 0,
            bill_coveragefrom: '',
            bill_coverageto: '',
            presentReadingDate: '',
            previousReadingDate: ''
        }
    }

    
    getBill = () =>{
        var params = new URLSearchParams();
        params.append("id", this.props.location.state.id);
        axios.post('http://'+Ip.IP+'/billing/get', params)
        .then(response => 
            this.setState({expense_lists: response.data[0].costs,items: response.data[0]}));
            
    }
    componentDidMount(){
        this.getBill();
    }

    render (){
        // if(this.state.items.length === 0){
            
            
        //     return <div><Form.Control type="file" onChange={(e)=> {
        //         const file = e.target.files[0];
        //         this.readExcel(file);
        //     }}></Form.Control></div>;
        // }
        return (
            <div>
                <Sidebar>
                    <Container style={{marginTop:'10px'}}>
                        <Row>
                            <Card style={{minWidth: '100%'}}>
                                <Card.Header style={{fontWeight:"bold", fontSize: "40px"}}>
                                    <Row>
                                        <Col md="6">View Bill</Col>
                                        <Col md="5"></Col>
                                        <Col md="1"><Link to="/Billing"><Button variant="default"><b>X</b></Button></Link></Col>
                                    </Row>
                                </Card.Header>
                            </Card>
                            <Form id="billForm" style={{width: "100%"}} onSubmit = {(e) => {
                                this.saveBill(e);
                            }}>        
                                <table class="table table-bordered" style={{backgroundColor: "#eeeded"}}>
                                <tr>
                                        <td>Bill Coverage</td>
                                        <td>{this.state.items.bill_coverage}</td>
                                        <td class="current_meter">
                                            <label style={{display: 'block'}}>Present Reading Date: {this.state.items.present_reading_date}</label>
                                        </td>
                                        <td style={{textAlign: "center"}}>
                                            <label>Previous Reading Date: {this.state.items.previous_reading_date}</label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Main water meter</td>
                                        <td>Current reading: {this.state.items.mainWatercurrent}</td>
                                        <td class="current_meter">Previous reading: {this.state.items.mainWaterprevious}</td>
                                            <td style={{textAlign: "center"}}><b>Total Consumption:</b> {this.state.items.total_consumption} </td>
                                    </tr>
                                    <tr>
                                        <td>Main Electricity meter:</td>
                                        <td>Current reading: {this.state.items.mainEleccurrent}</td>
                                        <td class="previous_meter">Previous reading: {this.state.items.mainElecprevious}</td>
                                        <td style={{textAlign: "center"}}><b>Total (kw) consumed:</b> {this.state.items.total_kw_consumed}</td>
                                    </tr>
                                    <tr>
                                        <td>Electricity Bill (VECO):</td>
                                        <td>Peso: <CurrencyFormat value={this.state.items.electric_bill} displayType={'text'} thousandSeparator={true} /></td>
                                        <td class="previous_meter">{this.state.items.kw}</td>
                                        <td style={{textAlign: "center"}}><b>Price/kw:</b> {this.state.items.price_kw}</td>
                                    </tr>
                                    <tr>
                                        <td>System Loss: {this.state.items.system_loss}</td>
                                        <td>Cubic loss per household: {this.state.items.loss_rate}</td>
                                        <td>No. of Household: {this.state.items.number_of_household}</td>
                                        <td> </td>
                                    </tr>
                                    <tr>
                                        <td>Computations:</td>
                                        <td colspan="3"></td>
                                    </tr>
                                    <tr>
                                        <td rowspan="3"></td>
                                        <td>Total KW used</td>
                                        <td>{this.state.items.total_kw_consumed}</td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>* Rate per kw</td>
                                        <td>{this.state.items.price_kw}</td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td><b>Total Electric bill (peso)</b></td>
                                        <td><b><CurrencyFormat value={this.state.items.total_electric_bill_peso} displayType={'text'} thousandSeparator={true} /></b></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td rowspan={1 + this.state.expense_lists?.length}>Add:</td>
                                        <td>Total Cubic meter</td>
                                        <td><CurrencyFormat value={this.state.items.mcwd} displayType={'text'} thousandSeparator={true} /></td>
                                        <td></td>
                                    </tr>
                                    {this.state.expense_lists?.map((data) => (
                                        <tr key={data.id}>
                                            <td>{data.expense_name}</td>
                                            <td><CurrencyFormat value={data.amount} displayType={'text'} thousandSeparator={true} /></td>
                                            <td></td>
                                        </tr>
                                    ))}
                                    <tr>
                                        <td></td>
                                        <td><b>Total Cost of water:</b></td>
                                        <td><b><CurrencyFormat value={this.state.items.total_cost_of_water} displayType={'text'} thousandSeparator={true} /></b></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>Divided by:</td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td rowspan="3"></td>
                                        <td>Total water consumption</td>
                                        <td>{this.state.items.total_consumption}</td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td><b>Price of water (m3)</b></td>
                                        <td><b>{this.state.items.rateCubic}</b></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    
                                </table>
                            </Form>  
                        </Row>
                    </Container>
                </Sidebar>
            </div>
        )
    }
}

export default Billing