import React, {Component} from 'react'
import  {Button, Form, Container, Card, Row, Col, Table} from 'react-bootstrap';
import axios from 'axios';
import {Link} from 'react-router-dom';
import Ip from './../Ip';
//
import Sidebar from './Sidebar';
class ViewHousehold extends Component{
    constructor(props){
        super(props);
        this.state = {
            name: '',
            mname: '',
            lname: '',
            lot: '',
            block: '',
            phase: '',
            meter_num: '',
            stub_num: '',
            paid_bill: [],
            unpaid_bill: []
        }
    }

    handleChange = (element) => {
        this.setState({
            [element.target.name]: element.target.value
        });
    }

    saveHousehold = (e) =>{
        e.preventDefault();
        const form = document.getElementById('householdForm');
        var params = new URLSearchParams();
        params.append("name", form.name.value);
        params.append("mname", form.mname.value);
        params.append("lname", form.lname.value);
        params.append("lot", form.lot.value);
        params.append("block", form.block.value);
        params.append("phase", form.phase.value);
        params.append("meter_num", form.meter_num.value);
        params.append("stub_num", form.stub_num.value);
        axios.post
        ('http://'+Ip.IP+'/household/save', params)
        .then(response => console.log({response})
            );
    }

    getSettings = () =>{
        var params = new URLSearchParams();
        params.append("id", this.props.location.state.id);
        axios.post('http://'+Ip.IP+'/household/get', params)
        .then(response => 
            this.setState({
                name: response.data[0].name,
                mname: response.data[0].mname,
                lname: response.data[0].lname,
                lot: response.data[0].lot,
                block: response.data[0].block,
                phase: response.data[0].phase,
                meter_num: response.data[0].meter_num,
                stub_num: response.data[0].stub_num,
                subdivision: response.data[0].subdivision,
                phone_number: response.data[0].phone_number
            }));
    }
    getPaidbillbyhousehold = () =>{
        var params = new URLSearchParams();
        params.append("id", this.props.location.state.id);
        axios.post('http://'+Ip.IP+'/billing/getPaidbillbyhousehold', params)
        .then(response => {
            console.log(response);
            this.setState({
                paid_bill: response.data
            })
            });
    }
    
    getUnpaidbillbyhousehold = () =>{
        var params = new URLSearchParams();
        params.append("id", this.props.location.state.id);
        axios.post('http://'+Ip.IP+'/billing/getUnpaidbillbyhousehold', params)
        .then(response => {
            console.log(response);
            this.setState({
                unpaid_bill: response.data
            })
            });
    }

    componentDidMount(){
        if (this.props.location.state.id != '') {
            this.getSettings();
            this.getPaidbillbyhousehold();
            this.getUnpaidbillbyhousehold();
        }
    }

    render(){
        return(
            <div>
                <Sidebar>
                    <div className="container">
                        <div className="row">
                                <Card style={{marginTop:"100px", width:"100%"}}>
                                                <Card.Header style={{fontSize: "40px", fontWeight: "bold"}}>
                                                    <Row>
                                                        <Col md="6">{this.props.location.state.title} Household</Col>
                                                        <Col md="5"></Col>
                                                        <Col md="1"><Link to="/Household"><Button variant="default"><b>X</b></Button></Link></Col>
                                                    </Row>
                                                </Card.Header>
                                                <Form id = "householdForm" style={{padding: "10px"}} onSubmit={(e) => {
                                                    if (window.confirm('Are you sure you wish to delete this item?'))
                                                    this.saveHousehold(e);
                                                }}>
                                                    
                                                    <Row>
                                                        <Col md="2">
                                                        <Form.Label>
                                                            <b>First name:</b>
                                                        </Form.Label>
                                                        </Col>
                                                        <Col md="4">
                                                        <Form.Control type="text" placeholder="Enter Firstname" value={this.state.name} readOnly name="name" onChange={(e) => {
                                                            this.handleChange(e);
                                                        }}></Form.Control>
                                                        </Col>
                                                        <Col md="2"><b>Block:</b></Col>
                                                        <Col md="4"><Form.Control type="text" placeholder="Enter Block" value={this.state.block} readOnly name="block" onChange={(e) => {
                                                            this.handleChange(e);
                                                        }}></Form.Control></Col>
                                                    </Row>
                                                    <Row style={{marginTop:'10px'}}>
                                                        <Col md="2">
                                                        <Form.Label>
                                                            <b>Middle name:</b>
                                                        </Form.Label>
                                                        </Col>
                                                        <Col md="4">
                                                        <Form.Control type="text" placeholder="Enter Middlename" value={this.state.mname} readOnly name="mname" onChange={(e) => {
                                                            this.handleChange(e);
                                                        }}></Form.Control>
                                                        </Col>
                                                        <Col md="2"><b>Lot:</b></Col>
                                                        <Col md="4"><Form.Control type="text" placeholder="Enter Lot" value={this.state.lot} readOnly name="lot" onChange={(e) => {
                                                            this.handleChange(e);
                                                        }}></Form.Control></Col>
                                                    </Row>
                                                    <Row  style={{marginTop:'10px'}}>
                                                        <Col md="2">
                                                        <Form.Label>
                                                            <b>Last name:</b>
                                                        </Form.Label>
                                                        </Col>
                                                        <Col md="4">
                                                        <Form.Control type="text" placeholder="Enter Lastname" readOnly value={this.state.lname} name="lname" onChange={(e) => {
                                                            this.handleChange(e);
                                                        }}></Form.Control>
                                                        </Col>
                                                        <Col md="2"><b>Phase:</b></Col>
                                                        <Col md="4"><Form.Control type="text" placeholder="Enter Phase" readOnly value={this.state.phase} name="phase" onChange={(e) => {
                                                            this.handleChange(e);
                                                        }}></Form.Control></Col>
                                                    </Row>
                                                    <Row  style={{marginTop:'10px'}}>
                                                        <Col md="2">
                                                        <Form.Label>
                                                            <b>Meter no.:</b>
                                                        </Form.Label>
                                                        </Col>
                                                        <Col md="4">
                                                        <Form.Control type="text" placeholder="Enter Meter no." readOnly name="meter_num" value={this.state.meter_num} onChange={(e) => {
                                                            this.handleChange(e);
                                                        }}></Form.Control>
                                                        </Col>
                                                        <Col md="2"><b>Stub out no.:</b></Col>
                                                        <Col md="4"><Form.Control type="text" placeholder="Enter Stub out no." value={this.state.stub_num} readOnly name="stub_num" onChange={(e) => {
                                                            this.handleChange(e);
                                                        }}></Form.Control></Col>
                                                    </Row>
                                                    <Row  style={{marginTop:'10px'}}>
                                                        <Col md="2">
                                                        <Form.Label>
                                                            <b>Phone no.:</b>
                                                        </Form.Label>
                                                        </Col>
                                                        <Col md="4">
                                                        <Form.Control type="text" placeholder="Enter Phone no." readOnly name="phone_number" value={this.state.phone_number} onChange={(e) => {
                                                            this.handleChange(e);
                                                        }}></Form.Control>
                                                        </Col>
                                                        <Col md="2"><b>Subdivision:</b></Col>
                                                        <Col md="4"><Form.Control type="text" placeholder="Enter Subdivision" value={this.state.subdivision} readOnly name="subdivision" onChange={(e) => {
                                                            this.handleChange(e);
                                                        }}></Form.Control></Col>
                                                    </Row>
                                                </Form>
                                        </Card>
                                        <Card  style={{width:"100%"}}>
                                            <ul class="nav nav-tabs">
                                                <li class="nav-item">
                                                    <a href="#home" class="nav-link active" data-toggle="tab">Paid Bills</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a href="#profile" class="nav-link" data-toggle="tab" style={{color:"red"}}>Unpaid Bills</a>
                                                </li>
                                            </ul>
                                            <div class="tab-content">
                                                <div class="tab-pane fade show active" id="home">
                                                    {this.state.paid_bill.map((data) => (
                                                       <Link to={{pathname:'/Ledger', state: {id:data.billed_iddd}}}><p style={{marginLeft: "8px"}}><a href="#">{data.bill_nos + " ("+ data.bill_coverage+")"}</a></p></Link>
                                                    ))}
                                                </div>
                                                <div class="tab-pane fade" id="profile">
                                                {this.state.unpaid_bill.map((data) => (
                                                    <Link key ={data.billed_iddd} to={{pathname:'/Ledger', state: {id:data.billed_iddd}}}><p style={{marginLeft: "8px"}}><a href="#">{data.bill_nos + " ("+ data.bill_coverage+")"}</a></p></Link>
                                                ))}
                                                </div>
                                            </div>
                                        </Card>
                                        
                                        
                        </div>
                    </div>
                </Sidebar>
            </div>
        )
    }
}

export default ViewHousehold