import React, {Component} from 'react';
import  {Button, Form, Container, Card, Row, Col, Table} from 'react-bootstrap';
import Select from 'react-select';
import axios from 'axios';
import Swal from 'sweetalert2'
import Sidebar from './Sidebar';
import CurrencyFormat from 'react-currency-format'
import Ip from './../Ip';

class Payment extends Component{
  constructor(props){
      super(props);
      this.state = {
          households : [],
          displayHeader: 'block',
          ledgerDisplay: 'none',
          bills: [],
          householdbill:[],
          paid_amount:0,
          advance_payment:0,
          total_due: 0,
          amount_to_pay: 0,
          bill_id: '',
          household_id: '',
          option: 'No bill found',
          totalExpenses: 0,
          expense_lists: [],
          showing: "",
          unpaidBills: [],
          grandTotalub: 0

      }
  }


    pay = (e) =>{
        e.preventDefault();
        if (parseFloat(this.state.amount_to_pay) < parseFloat(this.state.total_due)){
            Swal.fire("Insufficient amount", '', 'info')
        } else {
            this.setState({
                advance_payment: parseFloat(this.state.total_due).toFixed(2) <= 0 ? parseFloat(this.state.advance_payment).toFixed(2) - parseFloat(this.state.amount_to_pay) : (parseFloat(this.state.amount_to_pay).toFixed(2) - parseFloat(this.state.total_due).toFixed(2))
            }, () => {
                var params = new URLSearchParams();
                params.append("id", this.state.bill_id);
                params.append("advance_payment", this.state.advance_payment);
                params.append("paid_amount", this.state.amount_to_pay);
                params.append("paid_status", "1");
                params.append("household_id", this.state.household_id);
                axios.post('http://'+Ip.IP+'/billing/updateHouseholdBill', params)
                .then(response => {
                    this.setState({
                        displayHeader: 'none',
                        showing: 'none'}, 
                        () => {
                            window.print()
                            Swal.fire("Paid!", '', 'success')
                            this.props.history.push('/Home')  
                        });
                });
            });
        }
    }

    onChangebill = (e) =>{
        var bills = [...this.state.bills];
        var removeIndex = bills.map(function(item) { return item.id; }).indexOf(e.target.value);
        if (removeIndex != -1){
            bills.splice(removeIndex, 1);
        }
        var grandTotal = 0;
        var hehe = bills.map(function(item) { return grandTotal += parseFloat(item.amount_due) });

        this.setState({
            ledgerDisplay: 'block',
            unpaidBills: [...bills],
            grandTotalub : grandTotal
        });
        var params = new URLSearchParams();
        params.append("id", e.target.value);
        axios.post('http://'+Ip.IP+'/billing/getHouseholdBill', params)
        .then(response => {
            
            this.setState({householdbill: response.data[0]}, () => {
                var amount_due = parseFloat(this.state.householdbill.amount_due);
                var advance_payment = parseFloat(this.state.householdbill.advance);
                
                if (parseFloat(amount_due) > parseFloat(advance_payment)){
                    this.setState({
                        total_due: amount_due - advance_payment
                    });
                } else {
                    this.setState({
                        advance_payment: advance_payment - amount_due,
                        total_due: 0
                    });
                }
            })
        });
        
    }

    onChangeHousehold = (e) =>{
        this.setState({
            ledgerDisplay: 'none',
            household_id: e.value
        });
        
        var params = new URLSearchParams();
        params.append("id", e.value);
        axios.post('http://'+Ip.IP+'/billing/getAllBillbyHousehold', params)
        .then(response => {
            if (response.data.length > 0){
                this.setState({
                    option: "---SELECT---"
                });
            } else {
                this.setState({
                    option: "No bill found"
                });
            }
            this.setState({bills: response.data})
        });
    }

    getAllHousehold = () => {
        axios.get('http://'+Ip.IP+'/household/getAll')
        .then(response => {
            this.setState({
                households: response.data
            }, ()=>{
                
        console.log(this.state.households);
            });
        });
    }

    getSettings = () =>{
        axios.get('http://'+Ip.IP+'/settings/getLists')
        .then(response => 
            this.setState({expense_lists: response.data},() =>{
                var sum = response.data.reduce((total, currentValue) => total = total + parseInt(currentValue.amount),0);
                this.setState({
                    totalExpenses: sum
                })
            }));
            
    }

    handleChange = (element) => {
        this.setState({
            [element.target.name]: element.target.value
        });
    }

    componentDidMount = () => {
        this.getAllHousehold();
        this.getSettings();
    }

    render(){
        return (
        <Sidebar showing = {this.state.showing}>
            <div className="container">
                <div>
                    <div className="row">
                                    <div className="col-md-12 col-sm-12 col-lg-12 col-xs-12">
                                        
                                        <Card style={{marginTop:"100px", display: this.state.displayHeader}}>
                                                <Card.Header style={{fontSize: "40px", fontWeight: "bold"}}>
                                                    Payment Form
                                                </Card.Header>
                                                <Form style={{padding: "10px"}} >
                                                    
                                                    <Row>
                                                        <Col md="2">
                                                        <Form.Label>
                                                            <b>Select Household:</b>
                                                        </Form.Label>
                                                        </Col>
                                                        <Col md="4">
                                                        <Select onChange={(e) => {
                                                            this.onChangeHousehold(e)}}
                                                                options={this.state.households}
                                                            />
                                                        </Col>
                                                        <Col md="3"></Col>
                                                        <Col md="3"></Col>
                                                    </Row>
                                                    <Row >
                                                        <Col md="2">
                                                        <Form.Label style={{marginTop:'10px'}}>
                                                            <b>Bills to pay:</b>
                                                        </Form.Label>
                                                        </Col>
                                                        <Col md="4">
                                                            <select value={this.state.bills} class="form-control" name="bill_id" style={{marginTop:'10px'}} onChange={(e) => {
                                                                this.handleChange(e);
                                                                this.onChangebill(e);
                                                            }}>
                                                                <option>{this.state.option}</option>
                                                                {this.state.bills.map((data)=>(
                                                                    <option key ={data.id} value = {data.id}>{data.bill_nos +"     ("+ data.bill_coverage+")"}</option>
                                                                ))}
                                                            </select>
                                                        </Col>
                                                        <Col md="3"></Col>
                                                        <Col md="3"></Col>
                                                    </Row>
                                                    
                                                </Form>
                                        </Card>
                                        
                                        <div className="panel panel-default" style={{backgroundColor: "#eeeded", display: this.state.ledgerDisplay, textAlign: "center"}}>
                                            <div className="panel-heading" ><img src="./softouch.png" style={{height:"70px",width: "450px"}}></img></div>
                                                <div className="panel-body">
                                                    <div class="row" style={{margin: "0px"}}>
                                                        <div class="col-md-6 col-sm-6 col-lg-6 col-xs-12">
                                                            Name : {" "+this.state.householdbill.name +" "+this.state.householdbill.mname +" "+ this.state.householdbill.lname} <br></br>
                                                            Subdivision: {this.state.householdbill.subdivision} <br></br>
                                                            Address :{"Lot "+this.state.householdbill.lot +", Block "+this.state.householdbill.block +", (Phase "+ this.state.householdbill.phase+")"}<br></br>
                                                            Phone No.: {this.state.householdbill.phone_number} <br></br>
                                                        </div>
                                                        <div class="col-md-6 col-sm-6 col-lg-6 col-xs-12">
                                                            Bill No : {this.state.householdbill.bill_nos}<br></br>
                                                            Bill Date : {this.state.householdbill.date_created}<br></br>
                                                            Coverage : {this.state.householdbill.bill_coverage}
                                                            <br></br>
                                                            Meter No : {this.state.householdbill.meter_num}
                                                            <label class="control-label"  style={{color: "rgb(238, 237, 237)"}}>________</label>Stub out No. : {this.state.householdbill.stub_num} 
                                                            <br></br>
                                                        </div>
                                                    </div>
                                                    
                                                    <Form onSubmit={(e)=>{
                                                                                this.pay(e);
                                                                            }}>
                                                        <Row>
                                                            <Col md="1"></Col>
                                                            <Col md="10">
                                                            <table class="table-bordered">
                                                        <tr style={{ padding: "0 !important"}}>
                                                            <th style={{width:"15%"}}></th>
                                                            <th style={{width:"27.5%"}}>Service Period</th>
                                                            <th style={{width:"27.5%"}}>Meter Reading</th>
                                                            <th style={{width:"30%"}}>Due date</th>
                                                        </tr>
                                                        <tr>
                                                            <td style={{padding: "0rem !important"}}>Present Reading</td>
                                                            <td></td>
                                                            <td class="current_meter">{this.state.householdbill.current_reading}</td>
                                                            <td rowspan="3" style={{textAlign: "center"}}><b>{this.state.householdbill.due_date}</b></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Previous Reading</td>
                                                            <td></td>
                                                            <td class="previous_meter">{this.state.householdbill.previous_reading}</td>
                                                        </tr>
                                                        <tr>
                                                            <td><b>System Loss :</b> {this.state.householdbill.system_loss}</td>
                                                            <td><b>Cubic loss per household :</b>{this.state.householdbill.loss_rate}</td>
                                                            <td class="previous_meter"><b>No. of Household:</b>{this.state.householdbill.number_of_household}</td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2" style={{textAalign:"right"}}><b>Total Water Consumed</b></td>
                                                            <td class="total_meter">{this.state.householdbill.current_reading - this.state.householdbill.previous_reading}</td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="3">
                                                            <table  class="table-bordered">
                                                                    <tr>
                                                                        <td><b>Rate/Cubic Computation for the period :</b><b style={{textDecoration: 'underline'}}> {this.state.householdbill.bill_coverage}</b></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Water Pump Electricity Usage :<b > {this.state.householdbill.total_kw_consumed}</b> kwh  @ <b>{this.state.householdbill.price_kw}</b> kwh</td>
                                                                        <td> PHP <CurrencyFormat value={this.state.householdbill.total_electric_bill_peso} displayType={'text'} thousandSeparator={true} /></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Total Cubic meter</td>
                                                                        <td><CurrencyFormat value={this.state.householdbill.mcwd} displayType={'text'} thousandSeparator={true} /></td>
                                                                    </tr>
                                                                    {this.state.expense_lists.map((data) => (
                                                                        <tr key={data.id}>
                                                                            <td>{data.expense_name}</td>
                                                                            <td><CurrencyFormat value={data.amount} displayType={'text'} thousandSeparator={true} /></td>
                                                                        </tr>
                                                                    ))}
                                                                    <tr>
                                                                        <td><b style={{marginLeft: "510px"}}>Total Amount: </b></td>
                                                                        <td><b><CurrencyFormat value={this.state.householdbill.total_cost_of_water} displayType={'text'} thousandSeparator={true} /></b></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td><div style={{marginLeft: "190px"}}>Total Water Consumption (All household-based on reading)</div></td>
                                                                        <td>{this.state.householdbill.total_consumption}</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td><div style={{marginLeft: "250px"}}>Effective Rate/Cubic for this period</div></td>
                                                                        <td><b>{"PHP "+this.state.householdbill.rateCubic}</b></td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td> 
                                                                <table style={{width: "100%"}}> 
                                                                    <tr style={{width: "100%"}}>
                                                                        <th colspan="3">Unpaid Bills(s)</th>
                                                                    </tr>
                                                                    {this.state.unpaidBills.map((data)=>(
                                                                        <tr style={{width: "100%"}}>
                                                                            <td></td>
                                                                            <td>{data.bill_nos}</td>
                                                                            <td>{data.amount_due}</td>
                                                                        </tr>
                                                                    ))}
                                                                    <tr style={{width: "100%"}}>
                                                                        <td ></td>
                                                                        <td><b>Previous bill</b></td>
                                                                        <td ><b>{this.state.grandTotalub}</b></td>
                                                                    </tr>
                                                                </table>
                                                                <table style={{marginTop: "20px"}}> 
                                                                    <tr>
                                                                        <th></th>
                                                                        <th>m3</th>
                                                                        <th>rate/cubic</th>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Current bill</td>
                                                                        <td class="current_consumption">({this.state.householdbill.current_reading - this.state.householdbill.previous_reading} + {this.state.householdbill.loss_rate}) @ {this.state.householdbill.rateCubic}</td>
                                                                        <td class="total_water">{this.state.householdbill.amount_due}</td>
                                                                    </tr>    
                                                                    <tr>
                                                                        <td>Advance Payment</td>
                                                                        <td></td>
                                                                    <td>{this.state.householdbill.advance}</td>
                                                                    </tr>  
                                                                    <tr>
                                                                        <td colspan="2"><b>Current bill</b></td>
                                                                        <td class="total_water"><b><CurrencyFormat value={(this.state.total_due).toFixed(2)} displayType={'text'} thousandSeparator={true} /></b></td>
                                                                    </tr>  
                                                                    <tr>
                                                                        <td colspan="2"><input class="form-control" required name="amount_to_pay" value ={this.state.amount_to_pay} onChange={(e) => {
                                                                            this.handleChange(e);
                                                                        }} placeholder="Enter Amount"/></td>
                                                                        <td><button class="btn btn-success" type="submit" style={{display: this.state.displayHeader}}>Pay</button></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="2"><b>Total Payable Amount</b></td>
                                                                        <td class="total_water"><b><CurrencyFormat value={(this.state.total_due + this.state.grandTotalub).toFixed(2)} displayType={'text'} thousandSeparator={true} /></b></td>
                                                                    </tr>  
                                                                </table>
                                                                
                                                            </td>
                                                        </tr>
                                                            
                                                        </table>
                                                            <table>
                                                                    <tr>
                                                                        <td style={{fontSize: '13px'}}><b>IMPORTANT REMINDER: </b>Water connection will be disconnected in case you fail to settle your account  for two (2) months at SPDC. Reconnection fees apply.</td>
                                                                    </tr>
                                                                </table>
                                                            </Col>
                                                            <Col md="1"></Col>
                                                        </Row>
                                                    
                                                    </Form>
                                                </div>
                                        </div>
                                    </div>
                                </div>
                </div>
            </div>
        </Sidebar>
        
    )}
        
}

export default Payment