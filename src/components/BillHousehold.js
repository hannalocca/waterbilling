import React, {Component, useState} from 'react';
import {Button, Form, Container, Card, Row, Col, Table} from 'react-bootstrap';
import XLSX from 'xlsx';
import axios from 'axios';
import Swal from 'sweetalert2'
import CurrencyFormat from 'react-currency-format'
import {Link} from 'react-router-dom';

//components
import Sidebar from './Sidebar';
import Ip from './../Ip';

class BillingHousehold extends Component{
    constructor(props){
        super(props);
        this.state = {
            items : [],
            mainWatercurrent: 0,
            mainWaterprevious: 0,
            mainEleccurrent: 0,
            mainElecprevious: 0,
            totalConsumption: 0,
            totalkwConsumed: 0,
            electricityBillPeso: 0,
            electricityBillkw:0,
            electricityPriceKw:0,
            totalelectricityBillPeso: 0,
            rateCubic: 0,
            expense_lists: [],
            totalExpenses: 0,
            bill_info:[],
            systemLoss: 0,
            systemLossRate: 0,
            noOfHousehold: 0
        }
    }

    handleChange = (element) =>{
        const name = element.target.name;
        this.setState({
            [name] : element.target.value
        });
    }
    
    computeBreakdown = (e) =>{
        e.preventDefault();
        this.setState({
            totalConsumption : this.state.mainWatercurrent - this.state.mainWaterprevious,
            totalkwConsumed: this.state.mainEleccurrent - this.state.mainElecprevious,
            electricityPriceKw: this.state.electricityBillPeso / this.state.electricityBillkw
        },
        ()=> {
            this.setState({
                totalelectricityBillPeso: this.state.totalkwConsumed * this.state.electricityPriceKw
            }, () =>{
                this.setState({
                    rateCubic: (this.state.totalelectricityBillPeso + this.state.totalExpenses) / this.state.totalConsumption
                });
            });
        }); 
        
    }
    
    readExcel = (file) => {
        const promise = new Promise((resolve, reject) => {
            const fileReader = new FileReader();
            fileReader.readAsArrayBuffer(file);

            fileReader.onload = (e) =>{
                const bufferArray = e.target.result;

                const wb = XLSX.read(bufferArray, {type:'buffer'});

                const wsname = wb.SheetNames[0];

                const ws = wb.Sheets[wsname];

                const data = XLSX.utils.sheet_to_json(ws);

                resolve(data);
                var sum = data.reduce((total, currentValue) => total = total + (parseInt(currentValue.current)  - parseInt(currentValue.previous)),0);
                var totalSystemLoss = parseInt(this.state.bill_info.total_consumption) - parseInt(sum);
                var systemLosscubic = parseInt(totalSystemLoss) / parseInt(data.length);
                console.log(sum);
                this.setState({
                    noOfHousehold: data.length,
                    systemLoss: totalSystemLoss,
                    systemLossRate: (systemLosscubic).toFixed(2)
                });
            console.log(data.length)
            }
            fileReader.onerror = (error) => {
                reject(error);
            };
        });

        promise.then((d) => {
            const data = d;
            this.setState({
                items: data
              });
        });
    }


    getBill = () =>{
        var params = new URLSearchParams();
        params.append("id", this.props.location.state.id);
        axios.post('http://'+Ip.IP+'/billing/get', params)
        .then(response => 
            this.setState({expense_lists: response.data[0].costs,bill_info: response.data[0]}));
            
    }

    saveBilled = (e) => {
        e.preventDefault();
        Swal.fire({
            title: 'Do you want to save the changes?',
            showDenyButton: true,
            showCancelButton: true,
            confirmButtonText: `Save`,
            denyButtonText: `Don't save`,
            }).then((result) => {
                console.log(result.value);
            /* Read more about isConfirmed, isDenied below */
                if (result.value) {
                    const form = document.getElementById('householdForm');
                    var params = new URLSearchParams();
                    params.append("data", JSON.stringify(this.state.items));
                    params.append("rateCubic", this.state.bill_info.rateCubic);
                    params.append("bill_id", this.props.location.state.id);
                    params.append("noOfHousehold", this.state.noOfHousehold);
                    params.append("systemLoss", this.state.systemLoss);
                    params.append("systemLossRate", this.state.systemLossRate);
                    axios.post
                    ('http://'+Ip.IP+'/billing/saveBilled', params)
                    .then(response =>{ 
                            this.props.history.push('/Billing');
                            Swal.fire("Saved!", '', 'success');
                        });
                    
                } else{
                    Swal.fire('Changes are not saved', '', 'info')
                }
            })
    }

    componentDidMount(){
        this.getBill();
    }
    

    render (){
        // if(this.state.items.length === 0){
            
            
        //     return <div><Form.Control type="file" onChange={(e)=> {
        //         const file = e.target.files[0];
        //         this.readExcel(file);
        //     }}></Form.Control></div>;
        // }
        return (
            <div>
                <Sidebar>
                    <Container style={{marginTop:'10px'}}>
                        <Row>
                            <Card style={{minWidth: '100%'}}>
                                <Card.Header style={{fontWeight:"bold", fontSize: "40px"}}>
                                    
                                    <Row>
                                        <Col md="6">Bill Household</Col>
                                        <Col md="5"></Col>
                                        <Col md="1"><Link to="/Billing"><Button variant="default"><b>X</b></Button></Link></Col>
                                    </Row>
                                </Card.Header>
                                <Form>
                                    <Row>
                                        <Col md="3"></Col>
                                        <Col md="3">
                                            <Form.Label style={{fontWeight:"bold"}}>Import Household(s) Reading: </Form.Label>
                                        </Col>
                                        <Col md="3">
                                            <Form.Control type="file" onChange={(e)=> {
                                                const file = e.target.files[0];
                                                this.readExcel(file);
                                            }} className="btn btn-primary"></Form.Control>
                                        </Col>
                                        <Col md="3"></Col>
                                    </Row>
                                </Form>
                            </Card>
                            <Table striped bordered hover style={{marginTop:'50px', backgroundColor: 'white'}}>
                                <thead>
                                    <tr>
                                        <th>Bill date</th>
                                        <td>{this.state.bill_info.date_created}</td>
                                        <th>Bill No.</th>
                                        <td>{this.state.bill_info.bill_no}</td>
                                    </tr>
                                    <tr>
                                        <th>Bill Coverage</th>
                                        <td>{this.state.bill_info.bill_coverage}</td>
                                        <th>Total Electricity Usage (KWH)</th>
                                        <td>{this.state.bill_info.total_kw_consumed}</td>
                                    </tr>
                                    <tr>
                                        <th>Present Reading Date</th>
                                        <td>{this.state.bill_info.previous_reading_date}</td>
                                        <th>Rate Per KWH</th>
                                        <td>{this.state.bill_info.price_kw}</td>
                                    </tr>
                                    <tr>
                                        <th>Previous Reading Date</th>
                                        <td>{this.state.bill_info.present_reading_date}</td>
                                        <th>Total Accounted Water Consumption(m3)</th>
                                        <td>{this.state.bill_info.total_consumption}</td>
                                    </tr>
                                    <tr>
                                        <th>Rate/Cubic for this period</th>
                                        <td>{this.state.bill_info.rateCubic}</td>
                                        <th>Total System Loss</th>
                                        <td>{this.state.systemLoss}</td>
                                    </tr>
                                    <tr>
                                        <th>No. Of household</th>
                                        <td>{this.state.noOfHousehold}</td>
                                        <th>Cubic loss per Household</th>
                                        <td>{this.state.systemLossRate}</td>
                                    </tr>
                                </thead>             
                            </Table>
                            <Table striped bordered hover style={{marginTop:'50px', backgroundColor: 'white'}}>
                                <thead>
                                    <th>Household No.</th>
                                    <th>Household Full name</th>
                                    <th>Previous Reading</th>
                                    <th>Current Reading</th>
                                    <th>Total Consumption</th>
                                    <th>Total Cost</th>
                                </thead>
                                        {this.state.items.map(data => (
                                            <tr key={data.id}>
                                        
                                                <td>{data.id}</td>   
                                                <td>{data.fullname}</td> 
                                                <td>{data.previous}</td> 
                                                <td>{data.current}</td>
                                                 <td>({parseInt(data.current) - parseInt(data.previous)} + {parseInt(this.state.systemLossRate)}) @ <b>{this.state.bill_info.rateCubic}</b></td>  
                                                <td><b><CurrencyFormat value={(((parseInt(data.current) - parseInt(data.previous)) + parseFloat(this.state.systemLossRate)) * this.state.bill_info.rateCubic).toFixed(2)} displayType={'text'} thousandSeparator={true} /></b></td>
                                            </tr>  
                                        ))}    
                                                  
                            </Table>
                                    <Col md="4"></Col>.
                                    <Col md="4"></Col>
                                    <Col md="3">
                                        
                                        <Form onSubmit={(e) => {
                                            this.saveBilled(e);
                                        }}>     
                                                    <Button variant="success" type="submit" style={{marginLeft: '75px'}}>Save billed household</Button>
                                                    
                                        </Form>
                                    </Col>
                        </Row>
                    </Container>
                </Sidebar>
            </div>
        )
    }
}

export default BillingHousehold