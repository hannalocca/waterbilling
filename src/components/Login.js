import React, {Component} from 'react';
import {Button, Form, Container, Card, Row, Col} from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.css';
import axios from 'axios'
import Swal from 'sweetalert2'
import Ip from './../Ip';

class Login extends Component{

    constructor (props){
        super(props)
        this.state = {
            user: '',
            password: '',
            msg: ''
        }
    }

    handleChange = (element) => {
        this.setState({
            [element.target.name]: element.target.value
        });
    }


    login = (event) => {
        event.preventDefault();
        var params = new URLSearchParams();
        params.append("user", this.state.user);
        params.append("password", this.state.password);
        axios.post('http://'+Ip.IP+'/settings/login', params)
        .then(response => {
            console.log(response)
            if (response.data.length == 0){
                sessionStorage.setItem("userId","");
                this.setState({
                    msg: "Invalid Username/Password"
                });
                setTimeout(()=>{
                    this.setState({
                        msg: ""
                    });
                }, 3000);
            } else {
                sessionStorage.setItem("userId", response.data.id);
                Swal.fire("Welcome "+ response.data[0].username, '', 'success')
                this.props.history.push("/Home");
            }
        });
        
    }

    componentDidMount(){
    }

    render (){
        return (
            <div style={{marginTop: "250px"}}>
                <Container style={{padding: "15px"}} fluid>
                    <Row>
                    <Col md={4}></Col>
                    <Col md={4}>
                        <Card style={{ height: "320px", borderRadius: "30px 30px", opacity: "0.8"}}>
                        <Card.Header style={{height: "90px", fontSize: "50px", fontFamily: "Poppins-Bold", textAlign: "center"}}>
                        <label class="control-label">Softtouch Water Billing</label>
                        </Card.Header>
                        <Form style={{padding: "10px"}} onSubmit={this.login}>
                            <b><p style={{textAlign: "center", color: "rgb(204, 0, 0)"}}>{this.state.msg}</p></b>
                            <Form.Group>
                                <Form.Control type="text" onChange = {(e) => {
                                    this.handleChange(e);
                                }} placeholder="Username" name="user" required></Form.Control>
                            </Form.Group>
                            <Form.Group>
                                <Form.Control type="password" onChange = {(e) => {
                                    this.handleChange(e);
                                }} placeholder="Password" name="password" required></Form.Control>
                            </Form.Group>
                            
                            <Card.Footer>
                            <Button variant="danger" style={{width : "100%"}} type="submit">Login</Button>
                            </Card.Footer>
                        </Form>
                        </Card>
                    </Col>
                    <Col md={4}></Col>
                    </Row>
                    
                </Container>
            </div>
        )
    }
}

export default Login