import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import {Button, Form, Container, Card, Row, Col, Table, Modal} from 'react-bootstrap';
import * as XLSX from 'xlsx';
import {Link, Redirect, BrowserRouter as Router, Route} from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'jquery/dist/jquery.min.js';
//Datatable Modules
import "datatables.net-dt/js/dataTables.dataTables"
import "datatables.net-dt/css/jquery.dataTables.min.css"
import $ from 'jquery'; 
import axios from 'axios';
import { useHistory } from "react-router-dom";
import Swal from 'sweetalert2'
import Ip from './../Ip';

//components
import Sidebar from './Sidebar';
import Settings from './Settings';
class Household extends Component{
    constructor(props){
        super(props);
        this.state = {
            household_lists:[],
            modal: false
        }
    }

    objectToCSV = (data) => {
        const date = new Date();
        const datefile = date.getFullYear() + "" + (date.getMonth() + 1) + "" + date.getDate();
        var createXLSLFormatObj = [];
        var xlsHeader = ["id", "fullname", "previous", "current"];
        createXLSLFormatObj.push(xlsHeader);
        $.each(data, function(index, value) {
            const csvRows = [];
            $.each(value, function(ind, val) {
                csvRows.push(val);
            });
            createXLSLFormatObj.push(csvRows);
        });
        console.log(createXLSLFormatObj);
        var wb = XLSX.utils.book_new(),
            ws = XLSX.utils.aoa_to_sheet(createXLSLFormatObj);

        /* Add worksheet to workbook */
        XLSX.utils.book_append_sheet(wb, ws, "ws_name");
        XLSX.writeFile(wb, "Households"+datefile+".xlsx");
    }

    download = (data) => {
        
    }

    downloadAsExcel = () => {
        
        axios.post
        ('http://'+Ip.IP+'/household/exportHousehold')
        .then(response => {
            var data = response.data.map((data)=>({
                id : data.id,
                fullname :  data.name + ' '+data.mname+ ' '+ data.lname,
                previous : data.prev,
                current: ''
            }))
            const csvData = this.objectToCSV(data);
        });
        
    }

    editHousehold = (id) => {
        this.props.history.push({pathname: '/AddHousehold',
        state: {id: id, title:'Update'}});
    }

    viewHousehold = (id) => {
        this.props.history.push({pathname: '/ViewHousehold',
        state: {id: id, title:'View'}});
    }

    delete = (id) => {
        Swal.fire({
            title: 'Do you want to save the changes?',
            showDenyButton: true,
            showCancelButton: true,
            confirmButtonText: `Save`,
            denyButtonText: `Don't save`,
            }).then((result) => {
                console.log(result.value);
            /* Read more about isConfirmed, isDenied below */
                if (result.value) {
                    var params = new URLSearchParams();
                    params.append("id", id);
                    axios.post
                    ('http://'+Ip.IP+'/household/delete', params)
                    .then(response => {
                        
                        $('#example').DataTable().clear().destroy();
                        this.getAllHousehold()
                    });

                        
                    Swal.fire('Saved!', '', 'success')
                } else{
                    Swal.fire('Changes are not saved', '', 'info')
                }
            })
      
    }
    getAllHousehold = () => {
        axios.get('http://'+Ip.IP+'/household/getAll')
        .then(response => 
                this.setState({
                    household_lists: response.data
                }, () =>{
                    $(document).ready(function () {
                        $('#example').DataTable();
                    });
                    
                    this.table = $('#example').DataTable( {
                         retrieve: true,
                         data: response.data,
                         columns: [        
                            { data: '_name'},        
                            { data: 'lot'},       
                            { data: 'block'},       
                            { data: 'phase'},
                            { data: 'meter_num'},
                            { data: 'stub_num'},
                            { data: 'action'}   
                           ],
                        columnDefs: [
                            {
                            targets: [6],
                            width: 180,
                            className: "center",
                            createdCell: (td, cellData, rowData) =>
                            ReactDOM.render(
                                <div>
                                    <button
                                    id={rowData.id}
                                    class="btn btn-primary"
                                    onClick={() => {
                                    this.viewHousehold(rowData.action);
                                    }}
                                    > <i class="icon-fixed-width icon-eye-open"></i> </button>
                                    <button
                                    id={rowData.id}
                                    class="btn btn-info"
                                    onClick={(e) => {
                                    this.editHousehold(rowData.action);
                                    }}
                                    > <i class="icon-fixed-width icon-pencil"></i> </button>
                                    <button
                                    id={rowData.id}
                                    class="btn btn-danger"
                                    onClick={() => {
                                        this.delete(rowData.action);
                                    }}
                                    > <i class="icon-fixed-width icon-trash"></i> </button>
                                </div>
                                 , td ), },]
                            }); 
                        $('#example_wrapper').css( 'width', '100%' );
                        $('#example_wrapper').css( 'background-color', 'white');
                        $('#example_wrapper').css('padding', '5px');
                        $('#example_wrapper').css('padding-top', '20px');
                        $('#example').css( 'width', '100%');
                        this.table.columns.adjust().draw();
                    
                })
            );
    }
    

    componentDidMount() {
        //initialize datatable
        
        
        this.getAllHousehold();

     }
     
    render(){
        return (
            <div>
                <Sidebar>
                        
                    <Container>
                        <Row style={{marginTop: "5px"}}>
                            <Col md ="1"><Button style={{marginTop:'50px'}} variant="success" onClick={this.downloadAsExcel}><i class="icon-fixed-width icon-download"></i></Button></Col>
                            <Col md ="5"></Col>
                            <Col md ="3"></Col>
                            
                            <Col md ="1"></Col>
                            <Col md ="2"><Link to={{pathname: "/AddHousehold", state:{id: '', title: 'Add'}}}><Button style={{marginTop:'50px',marginLeft: '117px'}} variant="primary"><i class="icon-fixed-width icon-plus"></i></Button></Link></Col>
                            
                        </Row>
                        <Row style={{marginTop: "10px"}}>
                        <Card style={{width:"100%", backgroundColor: "white"}}>
                                <Card.Header><div style={{padding:'5px'}}><h1>Household(s) Management</h1></div></Card.Header>
                            </Card>
                            
                            <table id="example" class="display">
                                <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Lot</th>
                                        <th>Block</th>
                                        <th>Phase</th>
                                        <th>Meter No.</th>
                                        <th>Stub Out No.</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    
                                </tbody>
                            </table>
                        </Row>
                    </Container>
                </Sidebar>
            </div>
        )
    }
}

export default Household