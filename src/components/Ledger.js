import React, {Component} from 'react'
import {Button, Form, Container, Card, Row, Col, Table, Modal} from 'react-bootstrap';
import CurrencyFormat from 'react-currency-format'
import axios from 'axios';
import Ip from './../Ip';

class Ledger extends Component{
    constructor(props){
        super(props);
        this.state = {
            ledgerDisplay: 'block',
            householdbill: [],
            total_due: 0,
            expense_lists: [],
            unpaidBills: [],
            totalExpenses: 0,
            bills: [],
            unpaidBills: [],
            grandTotalub : 0
        }
    }

    getSettings = () =>{
        axios.get('http://'+Ip.IP+'/settings/getAll')
        .then(response => 
            this.setState({expense_lists: response.data},() =>{
                var sum = response.data.reduce((total, currentValue) => total = total + parseInt(currentValue.amount),0);
                this.setState({
                    totalExpenses: sum
                })
            }));
            
    }

    print = () => {
        window.print()
        this.props.history.push('/Household');
    }

    getAllBillbyHousehold = (id) =>{
        
        var params = new URLSearchParams();
        params.append("id", id);
        axios.post('http://'+Ip.IP+'/billing/getAllBillbyHousehold', params)
        .then(response => {
            var bills = [...response.data];
            var removeIndex = bills.map(function(item) { return item.id; }).indexOf(this.props.location.state.id);
            if (removeIndex > -1){
                bills.splice(removeIndex, 1);
            }
            var grandTotal = 0;
            var hehe = bills.map(function(item) { return grandTotal += parseFloat(item.amount_due) });

            this.setState({
                ledgerDisplay: 'block',
                unpaidBills: [...bills],
                grandTotalub : grandTotal
            }, () => {
                console.log(this.state.unpaidBills)
            });
        });
    }

    getBill = () => {
        
        var params = new URLSearchParams();
        params.append("id", this.props.location.state.id);
        axios.post('http://'+Ip.IP+'/billing/getHouseholdBill', params)
        .then(response => {
            console.log(response.data);
            this.setState({householdbill: response.data[0]}, () => {
                this.getAllBillbyHousehold(response.data[0].household_id)
                var amount_due = parseFloat(this.state.householdbill.amount_due);
                var advance_payment = parseFloat(this.state.householdbill.advance);
                    this.setState({
                        total_due: amount_due
                    });
            })
        });
    }

    componentDidMount(){
        this.getBill()
        this.getSettings()
        this.getAllBillbyHousehold()
    }


    render () {
        return (
            <div>
                <Container>
                    <div className="panel panel-default" style={{backgroundColor: "#eeeded", display: this.state.ledgerDisplay, textAlign: "center"}}>
                            <div className="panel-heading" ><img src="./softouch.png" style={{height:"70px",width: "450px"}}></img></div>
                                <div className="panel-body">
                                    <div class="row" style={{margin: "10px"}}>
                                        <div class="col-md-6 col-sm-6 col-lg-6 col-xs-12">
                                            Name : {" "+this.state.householdbill.name +" "+this.state.householdbill.mname +" "+ this.state.householdbill.lname} <br></br>
                                            Subdivision: {this.state.householdbill.subdivision} <br></br>
                                            Address :{"Lot "+this.state.householdbill.lot +", Block "+this.state.householdbill.block +", (Phase "+ this.state.householdbill.phase+")"}<br></br>
                                            Phone No.: {this.state.householdbill.phone_number} <br></br>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-lg-6 col-xs-12">
                                            Bill No : {this.state.householdbill.bill_nos}<br></br>
                                            Bill Date : {this.state.householdbill.date_created}<br></br>
                                            Coverage : {this.state.householdbill.bill_coverage}
                                            <br></br>
                                            Meter No : {this.state.householdbill.meter_num}
                                            <label class="control-label"  style={{color: "rgb(238, 237, 237)"}}>________</label>Stub out No. : {this.state.householdbill.stub_num} 
                                            <br></br>
                                        </div>
                                    </div>
                                    
                                    <Form onSubmit={(e)=>{
                                                                this.print(e);
                                                            }}>
                                    <table class="table-bordered">
                                        <tr>
                                            <th style={{width:"15%"}}></th>
                                            <th style={{width:"27.5%"}}>Service Period</th>
                                            <th style={{width:"27.5%"}}>Meter Reading</th>
                                            <th style={{width:"30%"}}>Due date</th>
                                        </tr>
                                        <tr>
                                            <td>Present Reading</td>
                                            <td></td>
                                            <td class="current_meter">{this.state.householdbill.current_reading}</td>
                                            <td rowspan="3" style={{textAlign: "center"}}><b>{this.state.householdbill.due_date}</b></td>
                                        </tr>
                                        <tr>
                                            <td>Previous Reading</td>
                                            <td></td>
                                            <td class="previous_meter">{this.state.householdbill.previous_reading}</td>
                                        </tr>
                                        <tr>
                                            <td><b>System Loss :</b> {this.state.householdbill.system_loss}</td>
                                            <td><b>Cubic loss per household :</b>{this.state.householdbill.loss_rate}</td>
                                            <td class="previous_meter"><b>No. of Household :</b>{this.state.householdbill.number_of_household}</td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" style={{textAalign:"right"}}><b>Total Water Consumed</b></td>
                                            <td class="total_meter">{this.state.householdbill.current_reading - this.state.householdbill.previous_reading}</td>
                                        </tr>
                                        <tr>
                                            <td colspan="3">
                                            <table class="">
                                                    <tr>
                                                        <td><b>Rate/Cubic Computation for the period :</b><b style={{textDecoration: 'underline'}}> {this.state.householdbill.bill_coverage}</b></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Water Pump Electricity Usage :<b > {this.state.householdbill.total_kw_consumed}</b> kwh  @ <b>{this.state.householdbill.price_kw}</b> kwh</td>
                                                        <td> PHP <CurrencyFormat value={this.state.householdbill.total_electric_bill_peso} displayType={'text'} thousandSeparator={true} /></td>
                                                    </tr>
                                                    
                                                    {this.state.expense_lists.map((data) => (
                                                        <tr key={data.id}>
                                                            <td>{data.expense_name}</td>
                                                            <td><CurrencyFormat value={data.amount} displayType={'text'} thousandSeparator={true} /></td>
                                                        </tr>
                                                    ))}
                                                    <tr>
                                                        <td><b style={{marginLeft: "510px"}}>Total Amount: </b></td>
                                                        <td><b><CurrencyFormat value={this.state.householdbill.total_cost_of_water} displayType={'text'} thousandSeparator={true} /></b></td>
                                                    </tr>
                                                    <tr>
                                                        <td><div style={{marginLeft: "190px"}}>Total Water Consumption (All household-based on reading)</div></td>
                                                        <td>{this.state.householdbill.total_consumption}</td>
                                                    </tr>
                                                    <tr>
                                                        <td><div style={{marginLeft: "250px"}}>Effective Rate/Cubic for this period</div></td>
                                                        <td><b>{"PHP "+this.state.householdbill.rateCubic}</b></td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td> 
                                            <table style={{width: "100%"}} class="table-bordered"> 
                                                                    <tr style={{width: "100%"}}>
                                                                        <th colspan="3">Unpaid Bills(s)</th>
                                                                    </tr>
                                                                    {this.state.unpaidBills.map((data)=>(
                                                                        <tr style={{width: "100%"}}>
                                                                            <td></td>
                                                                            <td>{data.bill_nos}</td>
                                                                            <td>{data.amount_due}</td>
                                                                        </tr>
                                                                    ))}
                                                                    <tr style={{width: "100%"}}>
                                                                        <td ></td>
                                                                        <td><b>Previous Bill</b></td>
                                                                        <td ><b><CurrencyFormat value={(this.state.grandTotalub).toFixed(2)} displayType={'text'} thousandSeparator={true} /></b></td>
                                                                    </tr>
                                                                </table>
                                                <table class="table-bordered"> 
                                                    <tr>
                                                        <th></th>
                                                        <th>m3</th>
                                                        <th>rate/cubic</th>
                                                    </tr>
                                                    <tr>
                                                        <td>Current bill</td>
                                                        <td class="current_consumption">({this.state.householdbill.current_reading - this.state.householdbill.previous_reading} + {(this.state.householdbill.loss_rate)}) @ {this.state.householdbill.rateCubic}</td>
                                                        <td class="total_water">{this.state.householdbill.amount_due}</td>
                                                    </tr> 
                                                    <tr>
                                                        <td></td>
                                                        <td><b>Current Bill</b></td>
                                                        <td class="total_water"><CurrencyFormat value={(this.state.total_due).toFixed(2)} displayType={'text'} thousandSeparator={true} /></td>
                                                    </tr>  
                                                    <tr>
                                                        <td></td>
                                                        <td><b>Amount Paid</b></td>
                                                        <td class="total_water"><CurrencyFormat value={this.state.householdbill.paid_amount} displayType={'text'} thousandSeparator={true} /></td>
                                                    </tr> 
                                                    <tr>
                                                        <td colspan="2"></td>
                                                        <td><button class="btn btn-success" type="submit">Print</button></td>
                                                    </tr>
                                                </table>
                                                
                                            </td>
                                        </tr>
                                            
                                        </table>
                                            <table>
                                                    <tr>
                                                        <td style={{fontSize: '13px'}}><b>IMPORTANT REMINDER:</b> Water connection will be disconnected in case you fail to settle your account  for two (2) months at SPDC. Reconnection fees apply.</td>
                                                    </tr>
                                                </table>
                                        </Form>
                                </div>
                        </div>
                </Container>
            </div>
        )
    }
}

export default Ledger