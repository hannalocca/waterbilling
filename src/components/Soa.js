import React, {Component} from 'react'
import Sidebar from './Sidebar';
import Select from 'react-select';
import axios from 'axios';
import  {Button, Form, Container, Card, Row, Col, Table} from 'react-bootstrap';
import CurrencyFormat from 'react-currency-format'
import Ip from './../Ip';

class Soa extends Component{
    constructor(props) {
        super(props);
        this.state = {
            billings: [],
            householdbill: [],
            total_due: 0,
            expense_lists: [],
            unpaidBills: [],
            display: '',
            color: "rgb(238, 237, 237)"
        }
    }

    getAllbilling = () => {
        axios.get('http://'+Ip.IP+'/billing/getAll')
        .then(response => {
            console.log(response.data)
            this.setState({
                billings: response.data
            });
        });
    }

    onChangebill = (e) => {
        console.log(e.value);
        var params = new URLSearchParams();
        params.append("id", e.value);
        axios.post('http://'+Ip.IP+'/billing/getBillReport', params)
        .then(response => {
            console.log(response.data[0])
            
            var householdBill = [];
            response.data[0].map((data, index)=> {
                // data.hehe = [{hoho : "hoho"}];
                var params = new URLSearchParams();
                params.append("id", data.household_id);
                axios.post('http://'+Ip.IP+'/billing/getAllBillbyHousehold', params)
                .then(householdUnpaidBills => {
                    data.unpaidBills = householdUnpaidBills.data;
                    householdBill.push(data);
                    this.setState({householdbill: householdBill, expense_lists: response.data.costs})
                });
                
                console.log(data)
            })
        });
    }

    // getSettings = () =>{
    //     axios.get('http://localhost/waterbillingApi/settings/getAll')
    //     .then(response => 
    //         this.setState({expense_lists: response.data},() =>{
    //             var sum = response.data.reduce((total, currentValue) => total = total + parseInt(currentValue.amount),0);
    //             this.setState({
    //                 totalExpenses: sum
    //             })
    //         }));
            
    // }

    prints = () => {
        this.setState({
            display: "none",
            color: "white"
        }, () => {
            window.print();
            this.props.history.push("/Home");
        });
    }
    
    componentDidMount(){
         this.getAllbilling();
        // this.getSettings();
    }

    render () {
        return (
            <div>
                <Sidebar>
                    <h1 style={{display: this.state.display}}>SOA</h1>
                    <Row>
                        <Col md="3" style={{display: this.state.display}}>
                                <Select onChange={(e) => {
                            this.onChangebill(e)}}
                                options={this.state.billings}
                            />
                        </Col>
                        <Col md="3">
                            <Button style={{display: this.state.display}} onClick = {this.prints}>Print</Button>
                        </Col>
                        <Col md="3"></Col>
                    </Row>
                    {this.state.householdbill.map((data, index)=>(
                        
                        <div className="panel panel-default" style={{marginTop:"0px",backgroundColor: "#eeeded", display: this.state.ledgerDisplay, textAlign: "center"}}>
                            <div className="panel-heading" ><img src="./softouch.png" style={{height:"70px",width: "450px"}}></img></div>
                                <div className="panel-body">
                                    <div class="row" style={{margin: "0px"}}>
                                        <div class="col-md-6 col-sm-6 col-lg-6 col-xs-12">
                                                Name : {" "+data.name +" "+data.mname +" "+ data.lname}<br></br>
                                                Subdivision : {data.subdivision} <br></br>
                                                Address :  {"Lot "+data.lot +", Block "+data.block +", (Phase "+ data.phase+")"}<br></br>
                                                Phone No : {data.phone_number}
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-lg-6 col-xs-12">
                                            Bill No : {data.bill_nos} <br></br>
                                            Bill Date : {data.date_created}<br></br>
                                            Coverage : {data.bill_coverage}<br></br>
                                            Meter No : {data.meter_num}
                                        <label class="control-label"  style={{color: this.state.color}}>________</label>Stub out No. : {data.stub_num} 
                                                    <br></br>
                                                
                                        </div>
                                    </div>
                                    
                                    <Form onSubmit={(e)=>{
                                                                this.pay(e);
                                                            }}>
                                        <Row>
                                            <Col md="1"></Col>
                                            <Col md="10">
                                            <table class="table-bordered">
                                        <tr style={{ padding: "0 !important"}}>
                                            <th style={{width:"15%"}}></th>
                                            <th style={{width:"27.5%"}}>Service Period</th>
                                            <th style={{width:"27.5%"}}>Meter Reading</th>
                                            <th style={{width:"30%"}}>Due date</th>
                                        </tr>
                                        <tr>
                                            <td style={{padding: "0rem !important"}}>Present Reading</td>
                                            <td></td>
                                            <td class="current_meter">{data.current_reading}</td>
                                            <td rowspan="3" style={{textAlign: "center"}}><b>{data.due_date}</b></td>
                                        </tr>
                                        <tr>
                                            <td>Previous Reading</td>
                                            <td></td>
                                            <td class="previous_meter">{data.previous_reading}</td>
                                        </tr>
                                        <tr>
                                            <td><b>System Loss :</b> {data.system_loss}</td>
                                            <td><b>Cubic loss per household :</b>{data.loss_rate}</td>
                                            <td class="previous_meter"><b>No. of Household :</b>{data.number_of_household}</td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" style={{textAalign:"right"}}><b>Total Water Consumed</b></td>
                                            <td class="total_meter">{data.current_reading - data.previous_reading}</td>
                                        </tr>
                                        <tr>
                                            <td colspan="3">
                                            <table  class="table-bordered">
                                                    <tr>
                                                        <td><b>Rate/Cubic Computation for the period :</b><b style={{textDecoration: 'underline'}}> {data.bill_coverage}</b></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Water Pump Electricity Usage :<b > {data.total_kw_consumed}</b> kwh  @ <b>{data.price_kw}</b> kwh</td>
                                                        <td> PHP <CurrencyFormat value={data.total_electric_bill_peso} displayType={'text'} thousandSeparator={true} /></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Total Cubic meter</td>
                                                        <td><CurrencyFormat value={data.mcwd} displayType={'text'} thousandSeparator={true} /></td>
                                                    </tr>
                                                    {this.state.expense_lists.map((data) => (
                                                        <tr key={data.id}>
                                                            <td>{data.expense_name}</td>
                                                            <td><CurrencyFormat value={data.amount} displayType={'text'} thousandSeparator={true} /></td>
                                                        </tr>
                                                    ))}
                                                    <tr>
                                                        <td><b style={{marginLeft: "410px"}}>Total Amount: </b></td>
                                                        <td><b><CurrencyFormat value={data.total_cost_of_water} displayType={'text'} thousandSeparator={true} /></b></td>
                                                    </tr>
                                                    <tr>
                                                        <td><div style={{marginLeft: "190px"}}>Total Water Consumption (All household-based on reading)</div></td>
                                                        <td>{data.total_consumption}</td>
                                                    </tr>
                                                    <tr>
                                                        <td><div style={{marginLeft: "250px"}}>Effective Rate/Cubic for this period</div></td>
                                                        <td><b>{"PHP "+data.rateCubic}</b></td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td> 
                                                <table style={{width: "100%"}}> 
                                                    <tr style={{width: "100%"}}>
                                                        <th colspan="3">Unpaid Bills(s)</th>
                                                    </tr>
                                                    {data.unpaidBills.map((datas)=>(
                                                        data.bill_nos !== datas.bill_nos ? 
                                                        <tr style={{width: "100%"}}>
                                                            <td></td>
                                                            <td>{datas.bill_nos}</td>
                                                            <td>{datas.amount_due}</td>
                                                        </tr> : <tr></tr>
                                                    ))}
                                                    <tr style={{width: "100%"}}>
                                                        <td ></td>
                                                        <td><b>Previous bill</b></td>
                                                        <td ><b>{this.state.grandTotalub}</b></td>
                                                    </tr>
                                                </table>
                                                <table style={{marginTop: "20px"}}> 
                                                    <tr>
                                                        <th></th>
                                                        <th>m3</th>
                                                        <th>rate/cubic</th>
                                                    </tr>
                                                    <tr>
                                                        <td>Current bill</td>
                                                        <td class="current_consumption">({data.current_reading - data.previous_reading} + {data.loss_rate}) @ {data.rateCubic}</td>
                                                        <td class="total_water">{data.amount_due}</td>
                                                    </tr>    
                                                    <tr>
                                                        <td>Advance Payment</td>
                                                        <td></td>
                                                    <td>{data.advance}</td>
                                                    </tr>  
                                                    <tr>
                                                        <td></td>
                                                        <td><b>Current bill</b></td>
                                                        <td class="total_water"><b><CurrencyFormat value={(data.amount_due - data.advance).toFixed(2)} displayType={'text'} thousandSeparator={true} /></b></td>
                                                    </tr>  
                                                </table>
                                                
                                            </td>
                                        </tr>
                                            
                                        </table>
                                            <table>
                                                    <tr>
                                                        <td style={{fontSize: '13px'}}><b>IMPORTANT REMINDER: </b>Water connection will be disconnected in case you fail to settle your account  for two (2) months at SPDC. Reconnection fees apply.</td>
                                                    </tr>
                                                </table>
                                            </Col>
                                            <Col md="1"></Col>
                                        </Row>
                                    
                                    </Form>
                                </div>
                        </div>
                        ))}
                </Sidebar>
            </div>
        )
    }

}

export default Soa