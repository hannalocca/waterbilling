import React, { Component } from 'react';
import  {Button, Form, Container, Card, Row, Col, Table} from 'react-bootstrap';
import axios from 'axios';
import Swal from 'sweetalert2'
//
import Sidebar from './Sidebar';
import Ip from './../Ip';
class Settings extends Component{
    constructor(){
        super();
        this.state = {
            expense_name: '',
            amount: 0,
            loaded: false,
            expense_lists: [],
            loading: 'none',
            btnSave: ''
        }
    }

    handleChange = (element) => {
        this.setState({
            [element.target.name]: element.target.value
        });
    }

    getSettings = () =>{
        axios.get('http://'+Ip.IP+'/settings/getLists')
        .then(response => this.setState({expense_lists: response.data}));
    }

    saveSettings = (event) => {
        event.preventDefault();
        

            Swal.fire({
                title: 'Do you want to save the changes?',
                showDenyButton: true,
                showCancelButton: true,
                confirmButtonText: `Save`,
                denyButtonText: `Don't save`,
                }).then((result) => {
                    console.log(result.value);
                /* Read more about isConfirmed, isDenied below */
                    if (result.value) {
                        this.setState({
                            loading: '',
                            btnSave: 'none'
                        });
                        const form = document.getElementById('settingsForm');
                        var params = new URLSearchParams();
                        params.append("expense_name", form.expense_name.value);
                        params.append("amount", form.amount.value);
                        axios.post
                        ('http://'+Ip.IP+'/settings/save', params)
                        .then(response => {
                            Swal.fire('Saved!', '', 'success')
                            this.setState({
                                loading: 'none',
                                btnSave: ''
                            },() => {
                                this.getSettings()
                            })
                        }    
                            );
                    } else{
                        Swal.fire('Changes are not saved', '', 'info')
                    }
                })
        
    }

    deleteSettings = (id) => {
        Swal.fire({
            title: 'Do you want to save the changes?',
            showDenyButton: true,
            showCancelButton: true,
            confirmButtonText: `Save`,
            denyButtonText: `Don't save`,
            }).then((result) => {
                console.log(result.value);
            /* Read more about isConfirmed, isDenied below */
                if (result.value) {
                    var params = new URLSearchParams();
                    params.append("id", id);
                    axios.post
                    ('http://'+Ip.IP+'/settings/delete', params)
                    .then(response => {
                            Swal.fire('Saved!', '', 'success')
                            this.getSettings()
                    });
                } else{
                    Swal.fire('Changes are not saved', '', 'info')
                }
            })
    }

    componentDidMount(){
        this.getSettings();
    }

    render(){
        return (
            <div>
                <Sidebar>
                    <Container>
                        <Card style={{marginTop:"100px"}}>
                            <Card.Header style={{fontSize: "40px", fontWeight: "bold"}}>
                                Settings (Expenses)
                            </Card.Header>
                            <Form style={{padding: "10px"}} onSubmit={(e) => {
                                this.saveSettings(e);
                            }} id="settingsForm">
                                
                                <Row>
                                    <Col md="1"></Col>
                                    <Col md="1">
                                    <Form.Label>Expense title:
                                    </Form.Label>
                                    </Col>
                                    <Col md="3">
                                        <Form.Control type="text" name="expense_name" value={this.state.title} onChange={(e) => {
                                            this.handleChange(e);
                                        }}></Form.Control>
                                    </Col>
                                    <Col md="1">Amount:</Col>
                                    <Col md="3"><Form.Control type="text" name="amount" value={this.state.amount} onChange={(e) => {
                                            this.handleChange(e);
                                        }}></Form.Control></Col>
                                    <Col md="2"><Button type="submit"><b style={{display: this.state.btnSave}}>Save</b><i class="icon-spinner icon-spin icon-large" style={{display: this.state.loading}}></i></Button></Col>
                                    <Col md="1"></Col>
                                </Row>
                                
                            </Form>
                        </Card>

                        <Table striped bordered hover style={{marginTop:'50px', backgroundColor: 'white'}}>
                                <thead>
                                    <th>Expense Name</th>
                                    <th>Amount</th>
                                    <th>Action</th>
                                </thead>
                                        {this.state.expense_lists.map(val =>(
                                            <tr key={val.id}>
                                                <td>{val.expense_name}</td>
                                                <td>{val.amount}</td>
                                                <td><Button disabled="true" variant="danger" onClick = {() => {
                                                    this.deleteSettings(val.id);
                                                }}><i class="icon-fixed-width icon-trash"></i></Button></td>
                                            </tr>
                                        ))}
                                        
                                                  
                            </Table>
                    </Container>
                </Sidebar>
            </div>
        )
    }
}

export default Settings